<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template {

    var $template_data = array();

    public function set($name, $value) {
        $this->template_data[$name] = $value;
    }

    public function view($view = '', $template = 'template', $view_data = array(), $return = FALSE) {
        $this->template_data = array_merge($this->template_data, $view_data);
        $this->CI = & get_instance();

        /*
         * Alert 
         */
        $this->setAlerts();

        /*
         * Post 
         */
        $this->setPosts();

        /*
         * Denied 
         */
        $this->setDenieds();



        if (!$template)
            return $this->CI->load->view($view, $this->template_data, $return);

        $this->set('contents', $this->CI->load->view($view, $this->template_data, TRUE));

        $dir = explode('/', $view);
        return $this->CI->load->view((count($dir) >= 2) ? $dir[0] . '/' . $template : $template, $this->template_data, $return);
    }

    private function setAlerts() {

        $alert = $this->CI->session->flashdata('alert');
        if ($alert)
            $this->set('alert', $alert);
    }

    private function setDenieds() {

        $denied = $this->CI->session->flashdata('denied');
        if ($denied)
            $this->set('denied', $denied);
    }

    private function setPosts() {

        $post = $this->CI->session->flashdata('post');
        if ($post)
            $this->set('post', $post);
    }

}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */