<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FilterList {

    private $CI;
    private $limit;
    private $offset;
    private $orderBy;
    private $ascDesc;
    private $defaultOrderBy;
    private $defaultAscDesc;

    public function __construct() {
        $this->CI = &get_instance();
        

        
        $this->limit = (isset($_POST['limit']) && $_POST['limit'] > 0) ? $_POST['limit'] : 15;
        $this->offset = (isset($_POST['limit']) && $_POST['offset'] > 0) ? $_POST['offset'] : 0;
        $this->orderBy = isset($_POST['orderBy']) ? $_POST['orderBy'] : null;
        $this->ascDesc = isset($_POST['ascDesc']) ? $_POST['ascDesc'] : null;
    }

    public function getPostFilters($filters = array()) {
        $return = array();
        if (count($filters) > 0) {
            foreach ($filters as $filter) {
                $return[$filter] = '';
                if (isset($_POST['filter_' . $filter]))
                    $return[$filter] = $_POST['filter_' . $filter] == 'null' ? null : $_POST['filter_' . $filter];
            }
        }
        return $return;
    }

    public function getLimit() {
        return $this->limit;
    }

    public function getOffset() {
        return $this->offset;
    }

    public function setOrderBy($orderBy) {
        $this->orderBy = $orderBy;
    }

    public function setDefaultOrderBy($orderBy) {
        $this->defaultOrderBy = $orderBy;
    }

    public function getOrderBy() {
        if (is_null($this->orderBy))
            return is_null($this->defaultOrderBy) ? '1' : $this->defaultOrderBy;
        else
            return $this->orderBy;
    }

    public function setAscDesc($ascDesc) {
        $this->ascDesc = $ascDesc;
    }

    public function setDefaultAscDesc($ascDesc) {
        $this->defaultAscDesc = $ascDesc;
    }

    public function getAscDesc() {
        if (is_null($this->ascDesc))
            return is_null($this->defaultAscDesc) ? 'ASC' : $this->defaultAscDesc;
        else
            return $this->ascDesc;
    }

}

/* End of file FilterList.php */
/* Location: ./system/application/libraries/FilterList.php */