<?php

class SiteController extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_admin_model', 'adminModel');
        $config = $this->adminModel->getById(1);
        
        $this->template->set('config', $config);
        $this->template->set('fones', $config['fones']);
        $this->template->set('facebook', $config['facebook']);
        $this->template->set('email', $config['email']);
        $this->template->set('rodape', $config['rodape']);
    }

}
