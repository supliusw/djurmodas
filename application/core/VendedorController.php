<?php

class VendedorController extends PermController {

    public $ADMIN;
    public $diretorio = 'vendedor/';

    public function __construct() {
        parent::__construct();

        if ($this->router->directory != 'common/' && $this->router->directory != $this->diretorio) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => 'Você não possui acesso a esse diretório'));
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->VENDEDOR = $this->perfilCorrente['fk_vendedor']['detail'];
        $this->template->set('VENDEDOR', $this->VENDEDOR);
    }

}
