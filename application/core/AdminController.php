<?php

class AdminController extends PermController {

    public $ADMIN;
    public $diretorio = 'admin/';

    public function __construct() {
        parent::__construct();

        if ($this->router->directory != 'common/' && $this->router->directory != $this->diretorio) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => 'Você não possui acesso a esse diretório'));
            redirect($_SERVER['HTTP_REFERER']);
        }


        $this->ADMIN = $this->perfilCorrente['fk_admin']['detail'];

        $this->template->set('ADMIN', $this->ADMIN);
    }

}
