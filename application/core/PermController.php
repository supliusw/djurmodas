<?php

class PermController extends CI_Controller {

    public $usuario;
    public $usuarioPerfis;
    public $usuarioRejeicoes = array();
    public $perfilCorrente;

    public function __construct() {
        parent::__construct();

        $usuario = $this->session->userdata('usuario');

        if ($this->router->class != 'login' || $usuario) {

            if (!$usuario)
                redirect('/common/login');

            try {
                $this->load->model('Tb_usuario_model', 'usuarioModel');
                $this->usuario = $this->usuarioModel->getById($usuario['id']);
            } catch (Exception $e) {
                $this->logout();
            }

            /*
             * Verificando se o usuário está incluso em algum grupo de permissão
             */
            if (isset($this->usuario['fk_grupo']) && $this->usuario['fk_grupo'] > 0) {
                $this->usuarioRejeicoes = $this->carregarRejeicoes($this->usuario['fk_grupo']);

                /*
                 * Verificando se possui acesso a determinado controller e/ou action
                 */
                if (!$this->verificaPermissao($this->router->class, $this->router->method)) {
                    $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => 'Você não possui permissão para acessar "' . $this->router->class . '/' . $this->router->method . '"'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }

            /*
             * Carregando perfis do usuário
             */
            $this->load->model('Tb_usuario_perfil_model', 'usuarioPerfilModel');
            $this->usuarioPerfis = $this->usuarioPerfilModel->getCompletByParam(array('fk_usuario' => $this->usuario['id']));

            /*
             * Verificando se o usuário possui algum perfil
             */
            if (empty($this->usuarioPerfis)) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => 'Usuário não possui perfil configurado. Consulte o administrador do sistema.'));
                $this->logout();
            }

            /*
             * Carregando perfil do usuário
             */
            $this->perfilCorrente = $this->session->userdata('perfilCorrente');

            /*
             * Se nenhum perfil foi carregado, selecionar o primeiro.
             */
            if (!$this->perfilCorrente) {
                $this->perfilCorrente = $this->usuarioPerfis[0];
                $this->session->set_userdata('perfilCorrente', $this->perfilCorrente);
            }
            
            $this->template->set('usuario', $this->usuario);
        }
    }

    public function logout() {
        $this->session->unset_userdata('usuario');
        $this->session->unset_userdata('perfilCorrente');
        redirect('/common/login');
    }

    protected function verificaPermissao($controller, $action = 'index') {
        foreach ($this->usuarioRejeicoes as $row)
            if ($row['controller'] == $controller && $row['action'] == $action)
                return false;

        return true;
    }

    protected function carregarRejeicoes($fk_grupo) {
        $this->db->select('c.controller, ca.action ');
        $this->db->from('tb_grupo_rejeicao gr');
        $this->db->join('tb_controller_action ca', 'gr.fk_controller_action = ca.id', 'left');
        $this->db->join('tb_controller c', 'ca.fk_controller = c.id', 'left');
        $this->db->where('gr.fk_grupo', $fk_grupo);

        $query = $this->db->get();
        return $query->result_array();
    }

}
