<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_contato_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_contato";
            $this->table_label = "TbContato";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "fk_pessoa" => array(
                    "label" => "fk_pessoa",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_pessoa_model",
                    "after_send" => ""
                ),
                "responsavel" => array(
                    "label" => "responsavel",
                    "rules" => "required|max_length[100]",
                    "after_send" => ""
                ),
                "email" => array(
                    "label" => "email",
                    "rules" => "max_length[100]",
                    "after_send" => ""
                ),
                "telefone" => array(
                    "label" => "telefone",
                    "rules" => "max_length[11]",
                    "after_send" => ""
                ),
                "celular" => array(
                    "label" => "celular",
                    "rules" => "max_length[11]",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
            
            $this->fields['telefone']['after_send'] = "unmask";
            $this->fields['celular']['after_send'] = "unmask";
        }
    }
