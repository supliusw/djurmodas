<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Smodel extends CI_Model {

    const IS_NOT_NULL = 'IS NOT NULL';
    const IS_NULL = 'IS NULL';

    protected $table = null;
    protected $table_label = null;
    protected $fields = array();

    public function __construct() {
        parent::__construct();
    }

    /*
     * Insert
     */

    public function insert($param = array()) {
        $array = $this->qualifying($param);
        $this->validate($array, false);

        $array['included_at'] = date('Y-m-d H:i:s');
        $array['updated_at'] = date('Y-m-d H:i:s');

        foreach ($array as $key => $value)
            $this->db->set($key, $value);

        $this->db->db_debug = FALSE;
        $this->db->insert($this->table);
        $this->db->db_debug = TRUE;

        if ($this->db->_error_message() == '') {
            $id = $this->db->insert_id();

            //Begin: Logger
            $this->log(array('chave' => $id, 'depois' => $array));
            //End: Logger

            return $id;
        } else
            throw new SModelException($this->db->_error_message());
    }

    /*
     * Update's
     */

    public function updateById($id, $param = array()) {
        return $this->updateWhere(array('id' => $id), $param);
    }

    public function updateWhere($where = array(), $param = array()) {
        $array = $this->qualifying($param);
        $this->validate($array, true);

        //Begin: Logger
        foreach ($where as $key => $value)
            $this->db->where($key, $value);
        $result = $this->db->get($this->table);
        $records = $result->result_array();
        //End: Logger


        foreach ($where as $key => $value)
            $this->db->where($key, $value);

        $this->db->db_debug = FALSE;
        $this->db->update($this->table, $array);
        $this->db->db_debug = TRUE;

        if ($this->db->_error_message() == '') {

            //Begin: Logger
            foreach ($records as $key => $value) {
                $antes = array_intersect_key($value, $array);
                $this->log(array('chave' => $value['id'], 'antes' => $antes, 'depois' => $array));
            }
            //End: Logger

            return true;
        } else
            throw new SModelException($this->db->_error_message());
    }

    /*
     * Delete's
     */

    public function deleteById($id, $logger = true) {
        return $this->deleteWhere(array('id' => $id), $logger);
    }

    public function deleteWhere($where = array(), $logger = true) {

        if ($logger) {
            foreach ($where as $key => $value)
                $this->db->where($key, $value);
            $result = $this->db->get($this->table);
            $records = $result->result_array();
        }

        $this->db->db_debug = FALSE;
        $this->db->delete($this->table, $where);
        $this->db->db_debug = TRUE;

        if ($this->db->_error_message() == '') {

            if ($logger)
                foreach ($records as $key => $value)
                    $this->log(array('chave' => $value['id'], 'antes' => $value, 'depois' => null));

            return true;
        } else
            throw new SModelException($this->db->_error_message());
    }

    /*
     * Get's
     */

    public function getById($id, $throwble = true) {
        $result = $this->getByParam(array('id' => $id));
        if (count($result) == 1) {
            return $this->qualifying($result[0]);
        } else {
            if ($throwble)
                throw new Exception("O registro solicitado nao existe");
            else
                return null;
        }
    }

    public function getCompletById($id, $niv = null, $throwble = true) {
        $result = $this->getById($id, $throwble);

        if (is_array($result)) {
            $i = 0;
            foreach ($this->fields as $name => $field) {
                if (isset($field['fk_table_model']) && isset($result[$name]) && $result[$name] > 0 && $field['fk_table_model'] != $niv) {
                    $this->load->model($field['fk_table_model']);
                    $id = $result[$name];
                    $result[$name] = array(
                        'id' => $result[$name],
                        'detail' => $this->{$field['fk_table_model']}->getCompletById($id, get_class($this))
                    );
                    $i++;
                }
            }
        }
        return $result;
    }

    public function getByParam($param = array(), $limit = null, $offset = null, $orderBy = null, $ascDesc = null) {
        $return = array();

        if (!is_null($orderBy) && !is_null($ascDesc))
            $this->db->order_by($orderBy, $ascDesc);

        $query = $this->db->get_where($this->table, $param, $limit, $offset);

        foreach ($query->result_array() as $row)
            $return[] = $this->qualifying($row);

        return $return;
    }

    public function getCompletByParam($param = array(), $limit = null, $offset = null, $orderBy = null, $ascDesc = 'ASC') {
        $return = array();

        if (!is_null($orderBy) && !is_null($ascDesc))
            $this->db->order_by($orderBy, $ascDesc);

        foreach ($param as $key => $value) {
            if ($value == Smodel::IS_NOT_NULL)
                $this->db->where("$key IS NOT NULL");
            else if ($value == Smodel::IS_NULL)
                $this->db->where("$key IS NULL");
            else
                $this->db->where($key, $value);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit);
            if (!is_null($offset)) {
                $this->db->limit($limit, $offset);
            }
        }

        $query = $this->db->get($this->table);

        foreach ($query->result_array() as $row)
            $return[] = $this->qualifying($row);

        if (is_array($return) && $return > 0) {
            foreach ($return as $i => $result) {
                if (is_array($result)) {
                    foreach ($this->fields as $name => $field) {
                        if (isset($field['fk_table_model']) && $result[$name] > 0) {
                            $this->load->model($field['fk_table_model']);
                            $id = $result[$name];
                            $return[$i][$name] = array(
                                'id' => $result[$name],
                                'detail' => $this->{$field['fk_table_model']}->getCompletById($id, get_class($this))
                            );
                        }
                    }
                }
            }
        }

        return $return;
    }

    /*
     * Log
     */

    private function log($param) {
        if (!isset($this->ignoreLog) || !$this->ignoreLog) {
            $CI = &get_instance();
            $usuario = $this->session->userdata('usuario');

            $param['tabela'] = $this->table;
            $param['tabela_label'] = $this->table_label;
            $param['antes'] = is_array($param['antes']) ? json_encode($param['antes']) : null;
            $param['depois'] = is_array($param['depois']) ? json_encode($param['depois']) : null;
            $param['fk_usuario'] = isset($usuario['id']) ? $usuario['id'] : null;
            $param['data_hora'] = date('Y-m-d H:i:s');

            unset($param['included_at']);
            unset($param['updated_at']);

            $this->db->insert('tb_log', $param);

            if (!is_null($param['antes']) && !is_null($param['depois'])) {
                $this->db->where('id', $param['chave']);
                $this->db->update($this->table, array('updated_at' => date('Y-m-d H:i:s')));
            }
        }
    }

    /*
     * Validation
     */

    public function validate($param = array(), $updating = false) {
        $this->validation = new Validation();
        $this->validation->set_data($param);
        foreach ($this->fields as $name => $field) {
            if (!in_array('primary_key', explode('|', $field['rules']))) {
                if (array_key_exists($name, $param) || !$updating) {
                    $this->validation->set_rules($name, $field['label'], isset($field['rules']) ? $field['rules'] : '');
                }
            }
        }
        if ($this->validation->run() === FALSE)
            throw new ValidationException('Validation error.', $this->validation->show_errors());
        else
            return true;
    }

    /*
     * Qualifying
     */

    public function qualifying($param = array()) {
        $return = array();
        foreach ($this->fields as $qualifier => $attr) {
            if (isset($param[$qualifier])) {
                $rules = explode('|', $attr['rules']);

                if (($param[$qualifier] == 'null' || strlen($param[$qualifier]) == 0 || $param[$qualifier] == '') && !in_array('integer', $rules) && !in_array('decimal', $rules)) {
                    $return[$qualifier] = null;
                } else {
                    if (isset($attr['after_send']) && $attr['after_send'] != '') {
                        $e = explode('|', $attr['after_send']);
                        if (count($e) > 0)
                            foreach ($e as $v)
                                $return[$qualifier] = call_user_func($v, $param[$qualifier]);
                    }else {
                        $return[$qualifier] = $param[$qualifier];

                        if (in_array('integer', $rules))
                            $return[$qualifier] = "$param[$qualifier]";

                        if (in_array('boolean', $rules)) {
                            if ($param[$qualifier] == 't')
                                $return[$qualifier] = 'true';

                            if ($param[$qualifier] == 'f')
                                $return[$qualifier] = 'false';
                        }

                        if (in_array('decimal', $rules)) {

                            $precision = 2;
                            if (preg_match("/precision\[(.*)\]/", $attr['rules'], $match)) {
                                $e = explode(',', $match[1]);
                                $precision = $e[1];
                            }
                            $return[$qualifier] = number_format(getFloat($param[$qualifier]), $precision, '.', '');
                        }

                        if (in_array('date_time', $rules) || in_array('date', $rules)) {
                            if (isDateBR($param[$qualifier]))
                                $return[$qualifier] = dateBrToUs($param[$qualifier], in_array('date_time', $rules) ? true : false);
                            else if (isDateUS($param[$qualifier]))
                                $return[$qualifier] = dateUsToBr($param[$qualifier], in_array('date_time', $rules) ? true : false);
                            else
                                $return[$qualifier] = $param[$qualifier];
                        }
                    }
                }
            }
        }
        return $return;
    }

    public function select_num_rows($query = null) {
        $query = $this->db->query($query);
        return $query->num_rows();
    }

    public function selectAll($orderBy = null, $ascDesc = 'asc') {
        $this->db->select('*');
        if (!is_null($orderBy))
            $this->db->order_by($orderBy, $ascDesc);

        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function count() {
        return $this->db->count_all($this->table);
    }

    public function avgByField($field) {
        $row = $this->db->select('avg(' . $field . ') as media')
                ->from($this->table)
                ->where($field . ' IS NOT NULL')
                ->get()
                ->row_array();

        if (is_null($row['media']))
            $media = 0.0;
        else
            $media = number_format($row['media'], 1, '.', '');

        return $media;
    }

}

/*
 * Exceptions
 */

class SModelException extends Exception {
    
}
