<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tb_usuario_perfil_model extends SModel {

    function __construct() {
        parent::__construct();
        $this->table = "tb_usuario_perfil";
        $this->table_label = "TbUsuarioPerfil";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "perfil" => array(
                    "label" => "perfil",
                    "rules" => "required|max_length[20]",
                    "after_send" => ""
                ),
                "fk_usuario" => array(
                    "label" => "fk_usuario",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_usuario_model",
                    "after_send" => ""
                ),
                "fk_admin" => array(
                    "label" => "fk_admin",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_admin_model",
                    "after_send" => ""
                ),
                "ativo" => array(
                    "label" => "ativo",
                    "rules" => "boolean",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
    }

    public function getPerfis() {

        $this->load->model('tb_admin_model', 'adminModel');

        $perfis['ADMIN'] = array(
            'fk' => 'fk_admin',
            'values' => $this->adminModel->selectAll()
        );

        return $perfis;
    }

    public function getCompletByParam($param = array(), $limit = null, $offset = null, $orderBy = null, $ascDesc = 'ASC') {
        $array = parent::getCompletByParam($param, $limit, $offset, $orderBy, $ascDesc);

        for ($i = 0; $i < count($array); $i++) {
            foreach ($array[$i] as $value) {
                if (isset($value['detail']['fk_pessoa'])) {
                    $array[$i]['fk_pessoa'] = $value['detail']['fk_pessoa'];
                    break;
                }
            }
        }
        
        return $array;
    }

}
