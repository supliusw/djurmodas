<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_controller_action_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_controller_action";
            $this->table_label = "TbControllerAction";
            $this->ignoreLog = true;

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "fk_controller" => array(
                    "label" => "fk_controller",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_controller_model",
                    "after_send" => ""
                ),
                "action" => array(
                    "label" => "action",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "descricao" => array(
                    "label" => "descricao",
                    "rules" => "",
                    "after_send" => ""
                ),
                "exibir" => array(
                    "label" => "exibir",
                    "rules" => "required|boolean",
                    "after_send" => ""
                ),
            );
        }
    }
