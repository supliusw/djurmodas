<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_produto_foto_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_produto_foto";
            $this->table_label = "TbProdutoFoto";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "fk_produto" => array(
                    "label" => "fk_produto",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_produto_model",
                    "after_send" => ""
                ),
                "path" => array(
                    "label" => "path",
                    "rules" => "required|max_length[255]",
                    "after_send" => ""
                ),
                "width" => array(
                    "label" => "width",
                    "rules" => "integer",
                    "after_send" => ""
                ),
                "height" => array(
                    "label" => "height",
                    "rules" => "integer",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
        }
    }
