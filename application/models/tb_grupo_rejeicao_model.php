<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_grupo_rejeicao_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_grupo_rejeicao";
            $this->table_label = "TbGrupoRejeicao";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "fk_grupo" => array(
                    "label" => "fk_grupo",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_grupo_model",
                    "after_send" => ""
                ),
                "fk_controller" => array(
                    "label" => "fk_controller",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_controller_model",
                    "after_send" => ""
                ),
                "fk_controller_action" => array(
                    "label" => "fk_controller_action",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_controller_action_model",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
        }
    }
