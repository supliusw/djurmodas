<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tb_admin_model extends SModel {

    function __construct() {
        parent::__construct();
        $this->table = "tb_admin";
        $this->table_label = "TbAdmin";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "fk_pessoa" => array(
                    "label" => "fk_pessoa",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_pessoa_model",
                    "after_send" => ""
                ),
                "fones" => array(
                    "label" => "fones",
                    "rules" => "",
                    "after_send" => ""
                ),
                "rodape" => array(
                    "label" => "rodape",
                    "rules" => "",
                    "after_send" => ""
                ),
                "facebook" => array(
                    "label" => "facebook",
                    "rules" => "",
                    "after_send" => ""
                ),
                "email" => array(
                    "label" => "email",
                    "rules" => "",
                    "after_send" => ""
                ),
                "quem_somos" => array(
                    "label" => "quem_somos",
                    "rules" => "",
                    "after_send" => ""
                ),
                "missao" => array(
                    "label" => "missao",
                    "rules" => "",
                    "after_send" => ""
                ),
                "objetivo" => array(
                    "label" => "objetivo",
                    "rules" => "",
                    "after_send" => ""
                ),
                "localizacao" => array(
                    "label" => "localizacao",
                    "rules" => "",
                    "after_send" => ""
                ),
                "latitude" => array(
                    "label" => "latitude",
                    "rules" => "max_length[30]",
                    "after_send" => ""
                ),
                "longitude" => array(
                    "label" => "longitude",
                    "rules" => "max_length[30]",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "pg_inicial_liganete" => array(
                    "label" => "pg_inicial_liganete",
                    "rules" => "",
                    "after_send" => ""
                ),
                "pg_inicial_coton" => array(
                    "label" => "pg_inicial_coton",
                    "rules" => "",
                    "after_send" => ""
                ),
                "pg_inicial_outros" => array(
                    "label" => "pg_inicial_outros",
                    "rules" => "",
                    "after_send" => ""
                ),
            );
    }

    public function selectAll($orderBy = 'nome', $ascDesc = 'asc') {
        return $this->db
                        ->select("p.nome as nome, a.*")
                        ->from('tb_admin a')
                        ->join('tb_pessoa p', 'a.fk_pessoa = p.id', 'left')
                        ->order_by($orderBy, $ascDesc)
                        ->get()
                        ->result_array();
    }

}
