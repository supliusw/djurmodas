<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_banner_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_banner";
            $this->table_label = "TbBanner";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "path" => array(
                    "label" => "path",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_At" => array(
                    "label" => "updated_At",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
        }
    }
