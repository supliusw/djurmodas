<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_produto_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_produto";
            $this->table_label = "TbProduto";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "categoria" => array(
                    "label" => "categoria",
                    "rules" => "required|max_length[45]",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "codigo" => array(
                    "label" => "codigo",
                    "rules" => "",
                    "after_send" => ""
                ),
                "composicao" => array(
                    "label" => "composicao",
                    "rules" => "",
                    "after_send" => ""
                ),
                "cores" => array(
                    "label" => "cores",
                    "rules" => "",
                    "after_send" => ""
                ),
                "grade" => array(
                    "label" => "grade",
                    "rules" => "",
                    "after_send" => ""
                ),
                "pagina_inicial" => array(
                    "label" => "pagina_inicial",
                    "rules" => "max_length[3]",
                    "after_send" => ""
                ),
                "fk_produto_foto" => array(
                    "label" => "fk_produto_foto",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_produto_foto_model",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
        }
    }
