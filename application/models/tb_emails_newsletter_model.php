<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_emails_newsletter_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_emails_newsletter";
            $this->table_label = "TbEmailsNewsletter";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "email" => array(
                    "label" => "email",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
        }
    }
