<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_endereco_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_endereco";
            $this->table_label = "TbEndereco";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "fk_cidade" => array(
                    "label" => "fk_cidade",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_cidade_model",
                    "after_send" => ""
                ),
                "fk_pessoa" => array(
                    "label" => "fk_pessoa",
                    "rules" => "required|integer",
                    "fk_table_model" => "Tb_pessoa_model",
                    "after_send" => ""
                ),
                "logradouro" => array(
                    "label" => "logradouro",
                    "rules" => "required|max_length[80]",
                    "after_send" => ""
                ),
                "numero" => array(
                    "label" => "numero",
                    "rules" => "required|max_length[20]",
                    "after_send" => ""
                ),
                "complemento" => array(
                    "label" => "complemento",
                    "rules" => "max_length[50]",
                    "after_send" => ""
                ),
                "bairro" => array(
                    "label" => "bairro",
                    "rules" => "required|max_length[100]",
                    "after_send" => ""
                ),
                "cep" => array(
                    "label" => "cep",
                    "rules" => "required|max_length[8]",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
            
            $this->fields['cep']['after_send'] = "unmask";
        }
    }
