<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_log_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_log";
            $this->table_label = "TbLog";
            $this->ignoreLog = true;

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "tabela" => array(
                    "label" => "tabela",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "tabela_label" => array(
                    "label" => "tabela_label",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "chave" => array(
                    "label" => "chave",
                    "rules" => "required|integer",
                    "after_send" => ""
                ),
                "antes" => array(
                    "label" => "antes",
                    "rules" => "",
                    "after_send" => ""
                ),
                "depois" => array(
                    "label" => "depois",
                    "rules" => "",
                    "after_send" => ""
                ),
                "data_hora" => array(
                    "label" => "data_hora",
                    "rules" => "required|date_time",
                    "after_send" => ""
                ),
                "fk_usuario" => array(
                    "label" => "fk_usuario",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_usuario_model",
                    "after_send" => ""
                ),
            );
        }
    }
