<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tb_grupo_model extends SModel {

    function __construct() {
        parent::__construct();
        $this->table = "tb_grupo";
        $this->table_label = "TbGrupo";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required|max_length[30]",
                    "after_send" => ""
                ),
                "fk_admin" => array(
                    "label" => "fk_admin",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_admin_model",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
    }

    public function getCompletByIdWithPermission($id) {
        $grupo = $this->getCompletById($id);
        $this->permissao($grupo);

        return $grupo;
    }

    public function permissao($grupo) {
        $CI = &get_instance();

        if (isset($this->ADMIN) && $grupo['fk_admin']['id'] != $this->ADMIN['id'])
            throw new Exception('Você nao ter permissao para acessar esse grupo de permissao');
    }

}
