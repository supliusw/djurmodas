<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_controller_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_controller";
            $this->table_label = "TbController";
            $this->ignoreLog = true;

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "controller" => array(
                    "label" => "controller",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required",
                    "after_send" => ""
                ),
                "descricao" => array(
                    "label" => "descricao",
                    "rules" => "",
                    "after_send" => ""
                ),
                "exibir" => array(
                    "label" => "exibir",
                    "rules" => "required|boolean",
                    "after_send" => ""
                ),
            );
        }
    }
