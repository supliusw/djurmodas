<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_cidade_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_cidade";
            $this->table_label = "TbCidade";
            $this->ignoreLog = true;

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required|max_length[80]",
                    "after_send" => ""
                ),
                "uf" => array(
                    "label" => "uf",
                    "rules" => "required|max_length[2]",
                    "after_send" => ""
                ),
            );
        }
    }
