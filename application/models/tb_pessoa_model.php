<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_pessoa_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_pessoa";
            $this->table_label = "TbPessoa";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "nome" => array(
                    "label" => "nome",
                    "rules" => "required|max_length[255]",
                    "after_send" => ""
                ),
                "razao_apelido" => array(
                    "label" => "razao_apelido",
                    "rules" => "max_length[255]",
                    "after_send" => ""
                ),
                "documento" => array(
                    "label" => "documento",
                    "rules" => "required|max_length[14]",
                    "after_send" => ""
                ),
                "fk_endereco" => array(
                    "label" => "fk_endereco",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_endereco_model",
                    "after_send" => ""
                ),
                "fk_contato" => array(
                    "label" => "fk_contato",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_contato_model",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
            
            $this->fields['documento']['after_send'] = "unmask";
        }
    }
