<?php
    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    class Tb_usuario_model extends SModel {

        function __construct() {
            parent::__construct();
            $this->table = "tb_usuario";
            $this->table_label = "TbUsuario";

            $this->fields = array(
                "id" => array(
                    "label" => "id",
                    "rules" => "primary_key|integer",
                    "after_send" => ""
                ),
                "usuario" => array(
                    "label" => "usuario",
                    "rules" => "required|max_length[255]",
                    "after_send" => ""
                ),
                "senha" => array(
                    "label" => "senha",
                    "rules" => "required|max_length[255]",
                    "after_send" => ""
                ),
                "fk_grupo" => array(
                    "label" => "fk_grupo",
                    "rules" => "integer",
                    "fk_table_model" => "Tb_grupo_model",
                    "after_send" => ""
                ),
                "included_at" => array(
                    "label" => "included_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
                "updated_at" => array(
                    "label" => "updated_at",
                    "rules" => "date_time",
                    "after_send" => ""
                ),
            );
        }
            }
