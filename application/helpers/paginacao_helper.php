<?php

function paginacao($param = array()) {
    $qtdMaxDisplayIndices = 15;
    $offset = $param['offset'];
    $limit = $param['limit'];
    $qtd = $param['qtd'];

    $anterior = $offset - $limit;
    $proximo = $offset + $limit;

    $anterior = ($anterior < 0) ? '' : $anterior;
    $proximo = ($proximo < 0) ? '' : $proximo;

    $html = '<div class="row">
        <div class="pull-left">(Qtd. de registros: ' . $qtd . ')</div>
                        <ul class="pagination pull-right">';


    if ($limit <= 0)
        $limit = 1;

    $atual = ceil($offset / $limit);
    $qtdPaginas = ceil($qtd / $limit);

    if ($atual != 0)
        $html .= '<li class="paginacao p_prev" itemref="' . $anterior . '"><a href="#">&laquo;</a></li> ';
    else
        $html .= '<li><a href="#">&laquo;</a></li>';

    $inicio = $atual - 5;
    if ($inicio < 0) {
        $inicio = 0;
        $off = 0;
    } else {
        $off = ($inicio - 1) * $limit;
    }

    $fim = $inicio + $qtdMaxDisplayIndices;
    $fim = ($fim > $qtdPaginas) ? $qtdPaginas : $fim;

    for ($i = $inicio; $i < $fim; $i++) {
        if ($atual == $i)
            $aux = 'active';
        else
            $aux = '';
        $html .= '<li class="paginacao p_indice ' . $aux . '" itemref="' . $off . '"><a href="#">' . ($i + 1) . '</a></li> ';
        $off += $limit;
    }

    if ($qtdPaginas > 1 && $atual != $qtdPaginas - 1)
        $html .= ' <li class="paginacao p_next" itemref="' . $proximo . '"><a href="#">&raquo;</a></li> ';
    else
        $html .= '<li><a>&raquo;</a></li>';

    $html .= '</ul></div>';
    echo $html;
}

?>
