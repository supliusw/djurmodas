<?php

//Define comum masks
define('MASK_TELEFONE', 'hybrid');
define('MASK_CPF', '###.###.###-##');
define('MASK_CNPJ', '##.###.###/####-##');
define('MASK_CEP', '##.###-###');

function format_destacar($texto, $destaque, $print = true) {
    $valor = (string) '<span style="background-color:yellow; color:#000;">' . $destaque . '</span>';
    $str = str_ireplace($destaque, $valor, $texto);
    if ($print)
        echo $str;
    else
        return $str;
}

function format_money($n, $p = false) {
    $precision = substr(strrchr($n, "."), 1);

    if (!$p) {
        if ((int) $precision == 0 || strlen($precision) == 1)
            return number_format($n, 2, ',', '.');
        else
            return number_format($n, strlen($precision), ',', '.');
    } else
        return number_format($n, $p, ',', '.');
}

function format_documento_hybrid($doc) {
    return strlen($doc) == 11 ? format_mask($doc, MASK_CPF) : (strlen($doc) == 14 ? format_mask($doc, MASK_CNPJ) : $doc);
}

function format_unmask($txt) {
    return preg_replace('/[\-\|\(\)\/\. ]/', '', $txt);
}

function format_mask($txt, $mascara) {
    if ($mascara == MASK_TELEFONE)
        $mascara = strlen($txt) == '10' ? '(##)####-####' : '(##)#####-####';

    if (empty($txt))
        return '';
    $txt = format_unmask($txt);
    $qtd = 0;
    for ($x = 0; $x < strlen($mascara); $x++) {
        if ($mascara[$x] == "#")
            $qtd++;
    }

    if ($qtd > strlen($txt)) {
        $txt = str_pad($txt, $qtd, "0", STR_PAD_LEFT);
    }

    if ($txt <> '') {
        $string = str_replace(" ", "", $txt);
        for ($i = 0; $i < strlen($string); $i++) {
            $pos = strpos($mascara, "#");
            $mascara[$pos] = $string[$i];
        }
        return $mascara;
    }
    return $txt;
}

function strToFloat($value) {
    $value = str_replace('R$', '', $value);
    return floatval(preg_replace('#^([-]*[0-9\.,\' ]+?)((\.|,){1}([0-9-]{1,2}))*$#e', "str_replace(array('.', ',', \"'\", ' '), '', '\\1') . '.\\4'", $value));
}

function format_removeBreakLines($str) {
    $output = str_replace(array("\r\n", "\r"), "\n", preg_replace("/\s+/", " ", $str));
    $lines = explode("\n", $output);
    $new_lines = array();
    foreach ($lines as $i => $line)
        if (!empty($line))
            $new_lines[] = trim($line);

    return implode($new_lines);
}

function unmaskMoneyBR($valor) {
    if (is_numeric($valor))
        return $valor;
    else {
        $valor = trim(str_replace('R$', '', $valor));
        if (strlen($valor) <= 3)
            return str_replace(',', '.', $valor);
        else {
            $valor = str_replace('.', '', $valor);
            return str_replace(',', '.', $valor);
        }
    }
}

function unmask($value) {
    return preg_replace('/[\-\|\(\)\/\. ]/', '', $value);
}

function isInteger($input) {
    return(ctype_digit(strval($input)));
}

function getFloat($pString) {
    if (strlen($pString) == 0) {
        return false;
    }
    $pregResult = array();

    $commaset = strpos($pString, ',');
    if ($commaset === false) {
        $commaset = -1;
    }

    $pointset = strpos($pString, '.');
    if ($pointset === false) {
        $pointset = -1;
    }

    $pregResultA = array();
    $pregResultB = array();

    if ($pointset < $commaset) {
        preg_match('#(([-]?[0-9]+(\.[0-9])?)+(,[0-9]+)?)#', $pString, $pregResultA);
    }
    preg_match('#(([-]?[0-9]+(,[0-9])?)+(\.[0-9]+)?)#', $pString, $pregResultB);
    if ((isset($pregResultA[0]) && (!isset($pregResultB[0]) || strstr($preResultA[0], $pregResultB[0]) == 0 || !$pointset))) {
        $numberString = $pregResultA[0];
        $numberString = str_replace('.', '', $numberString);
        $numberString = str_replace(',', '.', $numberString);
    } elseif (isset($pregResultB[0]) && (!isset($pregResultA[0]) || strstr($pregResultB[0], $preResultA[0]) == 0 || !$commaset)) {
        $numberString = $pregResultB[0];
        $numberString = str_replace(',', '', $numberString);
    } else {
        return false;
    }
    $result = (float) $numberString;
    return $result;
}

function printStatusFatura($status){
    switch ($status) {
        case 'EM ABERTO':
            echo '<span class="label label-warning">EM ABERTO</span>';
            break;
        
        case 'PAGO':
            echo '<span class="label label-success">PAGO</span>';
            break;
        
        case 'VENCIDO':
            echo '<span class="label label-danger">VENCIDO</span>';
            break;
        
        default:
            echo '<span class="label label-default">'.$status.'</span>';
            break;
    }
}

?>
