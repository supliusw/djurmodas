<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function isDateBR($dateBR, $hasTime = false) {
 
    if ($hasTime) {
        $a = explode(" ", $dateBR);
        $dateBR = $a[0];
    }
    $a = explode("/", $dateBR);
    if (count($a) != 3)
        return false;
    return checkdate($a[1], $a[0], $a[2]);
}

function isDateUS($dateUS) {
    $a = explode("-", $dateUS);
    if (count($a) != 3)
        return false;
    return checkdate($a[1], $a[2], $a[0]);
}

function diffDias($iniUS, $fimUS) {
    $time_inicial = strtotime($iniUS);
    $time_final = strtotime($fimUS);
    $diferenca = $time_final - $time_inicial;
    return (int) floor($diferenca / (60 * 60 * 24));
}

function diffDiasBR($iniBR, $fimBR) {
    $i = dateBrToUs($iniBR);
    $f = dateBrToUs($fimBR);
    return diffDias($i, $f);
}

function getStrMes($mes, $upper = false) {
    $mes = intval($mes);
    switch ($mes) {
        case 1: $str = "Janeiro";
            break;
        case 2: $str = "Fevereiro";
            break;
        case 3: $str = "Março";
            break;
        case 4: $str = "Abril";
            break;
        case 5: $str = "Maio";
            break;
        case 6: $str = "Junho";
            break;
        case 7: $str = "Julho";
            break;
        case 8: $str = "Agosto";
            break;
        case 9: $str = "Setembro";
            break;
        case 10: $str = "Outubro";
            break;
        case 11: $str = "Novembro";
            break;
        case 12: $str = "Dezembro";
            break;
        default: $str = "Erro";
            break;
    }

    if ($upper)
        $str = strtoupper($str, 'UTF-8');
    return $str;
}

function checkPeriodo($iniUS, $fimUS) {
    $i = strtotime($iniUS);
    $f = strtotime($fimUS);
    if ($i && $f)
        if ($i <= $f)
            return true;
    return false;
}

function checkPeriodoBR($iniBR, $fimBR) {
    $i = dateBrToUs($iniBR);
    $f = dateBrToUs($fimBR);
    return checkPeriodo($i, $f);
}

function dateBrToUs($dateBR, $time = false, $displayTime = true) {
    if (empty($dateBR))
        return "";
    if ($time) {
        $a = explode(" ", $dateBR);
        $dateBR = $a[0];
    }
    $e = explode("/", $dateBR);

    if ($displayTime)
        return trim($e[2] . '-' . $e[1] . '-' . $e[0] . ' ' . $a[1]);
    else
        return trim($e[2] . '-' . $e[1] . '-' . $e[0]);
}

function dateUsToBr($dateUS, $time = false, $displayTime = true) {
    if (empty($dateUS))
        return "";
    if ($time) {
        $a = explode(" ", $dateUS);
        $dateUS = $a[0];
    }
    $e = explode("-", $dateUS);
    if ($displayTime) {
        if (count($e) == 3)
            return trim($e[2] . '/' . $e[1] . '/' . $e[0] . ' ' . $a[1]);
        else
            return $dateUS;
    }else {
        if (count($e) == 3)
            return trim($e[2] . '/' . $e[1] . '/' . substr($e[0], 2, 2));
        else
            return $dateUS;
    }
}

?>
