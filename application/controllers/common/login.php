<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends PermController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_usuario_model', 'usuarioModel');
    }

    public function index() {

        if (is_array($this->perfilCorrente))
            redirect('/' . strtolower($this->perfilCorrente['perfil']) . '/home');

        $this->template->view('common/login.php', false);
    }

    public function acessar() {

        try {
            $usuario = $this->input->post('usuario');
            $senha = $this->input->post('senha');

            if (!$usuario || !$senha) {
                $this->template->view('common/login.php', false);
            } else {
                $usuarios = $this->usuarioModel->getByParam(array('usuario' => $usuario, 'senha' => md5($senha)));

                if (count($usuarios) == 1) {
                    $this->session->set_userdata('usuario', $usuarios[0]);
                } else
                    throw new Exception('Informe um usuário e senha válidos');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function escolherPerfil($perfil = null, $key = null) {

        if (!is_null($perfil) && !is_null($key) && $key > 0) {
            foreach ($this->usuarioPerfis as $i) {
                if ($i['perfil'] == $perfil && $i['id'] == $key) {
                    $this->session->set_userdata('perfilCorrente', $i);
                    redirect('/common/login');
                }
            }
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => 'Perfil nao encontrado'));
            redirect($_SERVER['HTTP_REFERER']);
        } else {

            $usuarioPerfis = array();
            foreach ($this->usuarioPerfis as $i)
                $usuarioPerfis[$i['perfil']][] = $i;

            $this->template->set('usuarioPerfis', $usuarioPerfis);
            $this->template->view('common/escolherPerfil.php', false);
        }
    }

}
