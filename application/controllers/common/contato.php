<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contato extends PermController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_pessoa_model', 'pessoaModel');
        $this->load->model('Tb_contato_model', 'contatoModel');
    }

    public function excluir($id = null) {
        try {
            $this->contatoModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Contato excluido com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER'].'#contatos');
    }

    public function salvar() {
        $dados = $this->input->post();

        try {
            if ($dados['idContato'] > 0) {
                $this->contatoModel->updateById($dados['idContato'], $dados);
            } else {
                $id = $this->contatoModel->insert($dados);
                $pessoa = $this->pessoaModel->getById($dados['fk_pessoa']);
                if (!$pessoa['fk_contato'] > 0)
                    $this->pessoaModel->updateById($pessoa['id'], array('fk_contato' => $id));
            }
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Contato salvo com sucesso!'));
        } catch (ValidationException $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            $this->session->set_flashdata('post', $this->input->post());
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            $this->session->set_flashdata('post', $this->input->post());
        }
        redirect($_SERVER['HTTP_REFERER'].'#contatos');
    }

    /*
     * Metodos auxiliares..
     */
}
