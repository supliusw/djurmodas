<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Util extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function formatJsonToCSV($json) {
        $return = $json;
        $array = json_decode($json, true);
        if ($array && count($array) > 0) {
            $return = "";
            foreach ($array as $key => $value) {
                if ($key != "tipo_documento")
                    $return .= '<strong>' . ucfirst($key) . ':</strong>&nbsp;' . strtoupper($value) . ', ';
            }
            $return = rtrim($return, ", ");
        }
        return $return;
    }

    public function consultaCep() {
        $post = $this->input->post();
        $this->load->library('PHPQuery', 'phpquery');
        $this->load->helper('util');

        $html = simpleCurl('http://m.correios.com.br/movel/buscaCepConfirma.do', array(
            'cepEntrada' => $post['cep'],
            'tipoCep' => '',
            'cepTemp' => '',
            'metodo' => 'buscarCep'
        ));


        phpQuery::newDocumentHTML($html, $charset = 'utf-8');

        $dados = array(
            'logradouro' => trim(phpQuery::pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html()),
            'bairro' => unaccents(trim(phpQuery::pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html())),
            'cep' => trim(phpQuery::pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
        );



        $aux = explode(" - ", $dados['logradouro']);
        if (count($aux) == 2)
            $dados['logradouro'] = $aux[0];

        $cidadeUF = explode("/", trim(phpQuery::pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()));



        $dados['cidade'] = unaccents(utf8_encode(trim($cidadeUF[0])));
        $dados['uf'] = trim($cidadeUF[1]);


        $this->displayJson($post['callback'], json_encode($dados));
    }

    public function getJsonUf() {
        $post = $this->input->post();
        $json = array("AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO");
        $this->displayJson($post['callback'], json_encode($json));
    }

    public function getJsonCidadeById($id = null) {
        $post = $this->input->post();
        $json = array();
        if (!is_null($id)) {
            $cidade = $this->db->get_where('tb_cidade', array('id' => $id))->result_array();
            $json = $cidade[0];
        }
        $this->displayJson($post['callback'], json_encode($json));
    }

    public function getJsonCidades($uf) {
        $post = $this->input->post();
        $json = $this->db->select('id, nome')
                ->from('tb_cidade')
                ->where('uf', $uf)
                ->order_by('nome')
                ->get()
                ->result_array();
        $this->displayJson($post['callback'], json_encode($json));
    }

    private function utf8_encode_all($dat) { // -- It returns $dat encoded to UTF8
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = $this->utf8_encode_all($d);
        return $ret;
    }

    private function displayJson($callback, $json) {
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

}
