<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pessoa extends PermController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_pessoa_model', 'pessoaModel');
    }

    public function cadastrar() {
        $dados = $this->input->post();

        try {
            $this->verificarTipo($dados['tipo']);

            $idPessoa = $this->pessoaModel->insert($dados);

            $id = $this->inserirTipo($dados['tipo'], array('fk_pessoa' => $idPessoa));

            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => $dados['tipo'] . ' cadastrado(a) com sucesso!'));

            redirect(dirname($_SERVER['HTTP_REFERER']) . '/ver/' . $id);
        } catch (ValidationException $e) {

            if ($idPessoa > 0)
                $this->db->query('DELETE FROM tb_pessoa WHERE id = ' . $idPessoa);

            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        } catch (Exception $e) {

            if ($idPessoa > 0)
                $this->db->query('DELETE FROM tb_pessoa WHERE id = ' . $idPessoa);

            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function vincular() {
        $dados = $this->input->post();

        try {

            $this->verificarTipo($dados['tipo']);

            $pessoa = $this->pessoaModel->getById($dados['fk_pessoa']);

            $id = $this->inserirTipo($dados['tipo'], array('fk_pessoa' => $pessoa['id']));

            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => $dados['tipo'] . ' cadastrado(a) com sucesso!'));
            redirect('admin/' . strtolower($dados['tipo']) . '/ver/' . $id);
        } catch (ValidationException $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function editar($id = null) {
        try {
            if (is_null($id))
                throw new Exception('Nenhum identificador informado');

            $dados = $this->input->post();

            $this->pessoaModel->updateById($id, $dados);
            $return = array('code' => 0);
        } catch (ValidationException $e) {
            $return = array('code' => 98, 'message' => $e->getDetailList());
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    public function excluir($id = null) {
        try {
            $this->pessoaModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Pessoa excluida com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function checkDocumento() {
        $dados = $this->input->post();
        try {
            if (is_null($dados['documento']))
                throw new Exception('Nenhum documento informado');

            $documento = unmask($dados['documento']);

            $sql = "SELECT p.*
                    FROM tb_pessoa p 
                    WHERE p.documento = '$documento' ";
            $query = $this->db->query($sql);
            $pessoas = $query->result_array();

            if (count($pessoas) != 1)
                throw new Exception('Nenhuma pessoa encontrada');


            $return = array(
                'code' => 0,
                'pessoa' => $pessoas[0]
            );
        } catch (ValidationException $e) {
            $return = array('code' => 98, 'message' => $e->getDetailList());
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    /*
     * Para marcar endereço/contato como principal
     */

    public function setParamPessoa($id = null) {
        try {
            $this->pessoaModel->updateById($id, $this->input->post());
            $return = array('code' => 0);
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }
        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    /*
     * Metodos auxiliares..
     */

    private function verificarTipo($tipo) {
        if ($tipo != 'Admin' &&
                $tipo != 'Cliente' &&
                $tipo != 'Vendedor')
            throw new Exception('Tipo de pessoa "' . $tipo . '" é inválido');
    }

    private function inserirTipo($tipo, $param) {

        switch ($tipo) {
            case 'Admin':
                $this->load->model('Tb_admin_model', 'adminModel');
                return $this->adminModel->insert($param);

            case 'Cliente':
                $this->load->model('Tb_cliente_model', 'clienteModel');
                return $this->clienteModel->insert($param);

            case 'Vendedor':
                $this->load->model('Tb_vendedor_model', 'vendedorModel');
                return $this->vendedorModel->insert($param);
        }

        return 0;
    }

}
