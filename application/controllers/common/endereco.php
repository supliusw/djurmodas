<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Endereco extends PermController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_pessoa_model', 'pessoaModel');
        $this->load->model('Tb_endereco_model', 'enderecoModel');
    }

    public function excluir($id = null) {
        try {
            $this->enderecoModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Endereço excluido com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER'].'#enderecos');
    }

    public function salvar() {
        $dados = $this->input->post();

        try {
            if ($dados['idEndereco'] > 0) {
                $this->enderecoModel->updateById($dados['idEndereco'], $dados);
            } else {
                $id = $this->enderecoModel->insert($dados);
                $pessoa = $this->pessoaModel->getById($dados['fk_pessoa']);
                if (!$pessoa['fk_endereco'] > 0)
                    $this->pessoaModel->updateById($pessoa['id'], array('fk_endereco' => $id));
            }
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Endereço salvo com sucesso!'));
        } catch (ValidationException $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            $this->session->set_flashdata('post', $this->input->post());
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            $this->session->set_flashdata('post', $this->input->post());
        }
        redirect($_SERVER['HTTP_REFERER'].'#enderecos');
    }

    /*
     * Metodos auxiliares..
     */
}
