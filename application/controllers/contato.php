<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contato extends SiteController {

    public function index() {
        $this->load->model('Tb_admin_model', 'adminModel');
        $config = $this->adminModel->getById(1);

        $this->template->set('localizacao', $config['localizacao']);
        $this->template->set('latitude', $config['latitude']);
        $this->template->set('longitude', $config['longitude']);
        $this->template->view('contato');
    }

    public function newsletter() {
        $dados = $this->input->post();

        try {
            $this->load->model('Tb_emails_newsletter_model', 'emailsNewsletterModel');
            $this->emailsNewsletterModel->insert($dados);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Seu email foi cadastrado com sucesso!'));
        } catch (ValidationException $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function enviar() {

        $dados = $this->input->post();

        try {

            if (strlen($dados['email']) == 0 || !filter_var($dados['email'], FILTER_VALIDATE_EMAIL))
                throw new Exception("Informe um email válido!");

            $this->load->library('email');

            $c['protocol'] = "smtp";
            $c['smtp_host'] = "ssl://smtp.djurmodas.com.br";
            $c['smtp_port'] = "587";
            $c['smtp_user'] = "contato@djurmodas.com.br";
            $c['smtp_pass'] = "ma34358636";
            $c['charset'] = "utf-8";
            $this->email->initialize($c);

            $this->email->from($dados['email']);

            $this->load->model('Tb_admin_model', 'adminModel');
            $config = $this->adminModel->getById(1);

            $this->email->to($config['email']);

            if (isset($dados['enviarCopia']))
                $this->email->bcc($dados['email']);

            $this->email->subject('Formulario de contato - Site');
            $this->email->message($dados['mensagem']);

            if ($this->email->send())
                $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Sua mensagem foi enviada com sucesso!'));
            else
                throw new Exception($this->email->print_debugger());
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}
