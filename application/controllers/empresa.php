<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empresa extends SiteController {

    public function index() {
        
        $this->load->model('Tb_admin_model', 'adminModel');
        $config = $this->adminModel->getById(1);
               
        $this->template->set('missao', $config['missao']);
        $this->template->set('objetivo', $config['objetivo']);
        $this->template->set('quemSomos', $config['quem_somos']);
        $this->template->view('empresa');
    }

}
