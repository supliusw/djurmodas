<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends SiteController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_banner_model', 'bannerModel');
        $this->load->model('Tb_produto_model', 'produtoModel');
    }

    public function index() {

        //Banners
        $banners = array();
        foreach ($this->bannerModel->selectAll() as $row)
            $banners[] = $row['path'];

        //Produtos
        $produtos = $this->db->select('p.id, pf.path, pf.id as idProdutoFoto')
                ->from('tb_produto p')
                ->join('tb_produto_foto pf', 'p.fk_produto_foto = pf.id', 'left')
                ->where('p.fk_produto_foto IS NOT NULL')
                ->where('p.pagina_inicial', 'SIM')
                ->get()
                ->result_array();


        $this->template->set('produtos', $produtos);
        $this->template->set('banners', $banners);

        $this->template->view('home');
    }

}
