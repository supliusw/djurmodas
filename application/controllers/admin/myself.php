<?php

class Myself extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->template->set('user', $this->usuarioModel->getById($this->usuario['id']));
        $this->template->view('common/usuario/myself.php', '../admin/template');
    }

}
