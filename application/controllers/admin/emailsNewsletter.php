<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EmailsNewsletter extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_emails_newsletter_model', 'emailsNewsletterModel');
    }

    public function index() {

        $this->load->library('filterlist');
        $this->filterlist->setDefaultOrderBy('e.email');
        $this->filterlist->setDefaultAscDesc('DESC');
        $filters = $this->filterlist->getPostFilters(array('like'));

        $sql = "SELECT e.*
                FROM tb_emails_newsletter e 
                WHERE true ";

        /*
         * Filtros..
         */
        if ($filters['like'] != '')
            $sql .= " AND (e.email LIKE '%" . $filters['like'] . "%' )";

        /*
         * Paginação..
         */
        $this->template->set('paginacao', array(
            'limit' => $this->filterlist->getLimit(),
            'offset' => $this->filterlist->getOffset(),
            'qtd' => $this->emailsNewsletterModel->select_num_rows($sql)
                )
        );

        /*
         * Ordenação..
         */
        $sql .= " ORDER BY " . $this->filterlist->getOrderBy() . " " . $this->filterlist->getAscDesc();
        $sql .= " LIMIT " . $this->filterlist->getLimit() . " OFFSET " . $this->filterlist->getOffset();
        $this->template->set('ordenacao', array(
            'orderBy' => $this->filterlist->getOrderBy(),
            'ascDesc' => $this->filterlist->getAscDesc()
        ));

        $query = $this->db->query($sql);
        $lista = $query->result_array();

        $this->template->set('lista', $lista);
        $this->template->set('filters', $filters);

        $this->template->view('admin/emailsNewsletter/index.php');
    }

}
