<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Empresa extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_admin_model', 'adminModel');
    }

    public function index() {
        $this->template->set('config', $this->adminModel->getById(1));
        $this->template->view('admin/empresa/index');
    }

    public function editar($id = null) {
        try {
            if (is_null($id))
                throw new Exception('Nenhum identificador informado');

            $dados = $this->input->post();

            $this->adminModel->updateById($id, $dados);
            $return = array('code' => 0);
        } catch (ValidationException $e) {
            $return = array('code' => 98, 'message' => $e->getDetailList());
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output((isset($callback) && !is_null($callback)) ? "{$callback}($json)" : $json);
    }

}
