<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuario extends AdminController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_usuario_model', 'usuarioModel');
        $this->load->model('Tb_usuario_perfil', 'usuarioPerfilModel');
    }

    public function index() {

        $this->load->library('filterlist');
        $this->filterlist->setDefaultOrderBy('u.usuario');
        $filters = $this->filterlist->getPostFilters(array('like'));

        $this->db->select('u.*, '
                . '(select count(up.id) from tb_usuario_perfil up where up.fk_usuario = u.id) as qtd ');
        $this->db->from('tb_usuario u');

        /*
         * Filtros..
         */
        if ($filters['like'] != '')
            $this->db->like('u.usuario', $filters['like']);

        /*
         * Paginação..
         */
        $db = clone $this->db;
        $this->template->set('paginacao', array(
            'limit' => $this->filterlist->getLimit(),
            'offset' => $this->filterlist->getOffset(),
            'qtd' => $db->count_all_results()
        ));

        /*
         * Ordenação..
         */
        $this->db->order_by($this->filterlist->getOrderBy(), $this->filterlist->getAscDesc());
        $this->db->limit($this->filterlist->getLimit(), $this->filterlist->getOffset());

        $this->template->set('ordenacao', array(
            'orderBy' => $this->filterlist->getOrderBy(),
            'ascDesc' => $this->filterlist->getAscDesc()
        ));

        /*
         * Consultando
         */
        $query = $this->db->get();
        $lista = $query->result_array();


        $this->template->set('lista', $lista);
        $this->template->set('filters', $filters);

        $this->template->view('admin/usuario/index.php');
    }

    public function cadastrar() {
        $dados = $this->input->post();

        if ($dados) {
            try {

                $dados['senha'] = md5($dados['senha']);

                $id = $this->usuarioModel->insert($dados);
                $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Usuário cadastrado com sucesso!'));
                redirect('/admin/usuario/ver/' . $id);
            } catch (ValidationException $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            } catch (Exception $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            }
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->template->view('admin/usuario/cadastrar.php');
        }
    }

    public function ver($id = null) {
        try {
            $usuario = $this->usuarioModel->getCompletById($id);

            $perfil = $this->usuarioPerfilModel->getPerfis();
            $perfisDoUsuario = $this->usuarioPerfilModel->getCompletByParam(array('fk_usuario' => $id));

            $this->template->set('perfisDoUsuario', $perfisDoUsuario);
            $this->template->set('perfis', $perfil);
            $this->template->set('dados', $usuario);
            $this->template->view('admin/usuario/ver.php');
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function editar($id = null) {
        try {
            $this->usuarioModel->getById($id);

            $dados = $this->input->post();

            if (isset($dados['senha']))
                $dados['senha'] = md5($dados['senha']);

            $this->usuarioModel->updateById($id, $dados);
            $return = array('code' => 0);
        } catch (ValidationException $e) {
            $return = array('code' => 98, 'message' => $e->getDetailList());
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    public function excluir($id = null) {
        try {
            $this->usuarioModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Usuário excluido com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER'] . '#usuarios');
    }

    /*
     * Perfis
     */

    public function addPerfil() {
        $dados = $this->input->post();

        try {
            $this->usuarioPerfilModel->insert($dados);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Perfil incluido com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function removerPerfil($id = null) {
        try {
            $this->usuarioPerfilModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Perfil excluido com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER'] . '#usuarios');
    }

    /*
     * Metodos auxiliares..
     */
}
