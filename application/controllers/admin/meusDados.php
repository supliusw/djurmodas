<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MeusDados extends AdminController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_admin_model', 'adminModel');
        $this->load->model('Tb_usuario_model', 'usuarioModel');
        $this->load->model('Tb_grupo_model', 'grupoModel');
        $this->load->model('Tb_endereco_model', 'enderecoModel');
        $this->load->model('Tb_contato_model', 'contatoModel');
    }

    public function index() {
        try {
            $admin = $this->adminModel->getCompletById($this->ADMIN['id']);
            $pessoa = $admin['fk_pessoa']['detail'];

            //Selecionando os usuarios ligados ao Admin
            $this->template->set('usuarios', $this->db->select("u.id as fk_usuario, u.usuario, up.id as fk_usuario_perfil, up.ativo as ativo_usuario_perfil, g.nome as grupo, u.fk_grupo")
                            ->from('tb_usuario_perfil up')
                            ->join('tb_usuario u', 'up.fk_usuario = u.id', 'left')
                            ->join('tb_grupo g', 'u.fk_grupo = g.id', 'left')
                            ->where('up.fk_admin', $this->ADMIN['id'])
                            ->order_by('u.usuario')
                            ->get()
                            ->result_array());

            /*
             * Selecionando as Grupos de permissao
             */
            $this->template->set('gruposPermissao', $this->grupoModel->getCompletByParam(array('fk_admin' => $this->ADMIN['id']), null, null, 'nome'));

            $this->template->set('enderecos', $this->enderecoModel->getCompletByParam(array('fk_pessoa' => $pessoa['id'])));
            $this->template->set('contatos', $this->contatoModel->getCompletByParam(array('fk_pessoa' => $pessoa['id'])));

            $this->template->set('idRef', $this->ADMIN['id']);

            $this->template->set('pessoa', $pessoa);
            $this->template->set('user', $this->usuarioModel->getById($this->usuario['id']));
            $this->template->view('admin/meusDados/index.php');
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /*
     * Metodos auxiliares
     */
}
