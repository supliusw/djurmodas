<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ProdutoFoto extends PermController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_produto_model', 'produtoModel');
        $this->load->model('Tb_produto_foto_model', 'produtoFotoModel');
    }

    public function cadastrar() {
        $dados = $this->input->post();

        if ($dados) {
            try {

                $config['upload_path'] = APPPATH . '../public_html/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = 'img' . date("YmdHis");

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('url_foto'))
                    throw new Exception($this->upload->display_errors());

                $data = $this->upload->data();

                $id = $this->produtoFotoModel->insert(array(
                    'path' => '/uploads/' . $data['orig_name'],
                    'fk_produto' => $dados['fk_produto'],
                    'width' => $data['image_width'],
                    'height' => $data['image_height']
                ));


                $this->produtoModel->updateById($dados['fk_produto'], array(
                    'fk_produto_foto' => $id
                ));

                $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Foto cadastrada com sucesso!'));
            } catch (ValidationException $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            } catch (Exception $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            }
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function excluir($id = null) {
        try {
            $this->produtoFotoModel->getById($id);
            $this->produtoFotoModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Foto excluida com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}
