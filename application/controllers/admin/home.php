<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_banner_model', 'bannerModel');
        $this->load->model('Tb_admin_model', 'adminModel');
    }

    public function index() {

        $this->template->set('banners', $this->bannerModel->selectAll());
        $this->template->set('config', $this->adminModel->getById(1));

        $this->template->view('admin/home');
    }

    public function editar($id = null) {
        try {
            if (is_null($id))
                throw new Exception('Nenhum identificador informado');

            $dados = $this->input->post();

            $this->adminModel->updateById($id, $dados);
            $return = array('code' => 0);
        } catch (ValidationException $e) {
            $return = array('code' => 98, 'message' => $e->getDetailList());
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output((isset($callback) && !is_null($callback)) ? "{$callback}($json)" : $json);
    }

    public function alterarFoto() {
        $dados = $this->input->post();

        if ($dados) {

            try {

                $config['upload_path'] = APPPATH . '../public_html/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['file_name'] = 'img' . date("YmdHis");

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('path'))
                    throw new Exception($this->upload->display_errors());

                $data = $this->upload->data();

                $this->adminModel->updateById(1, array(
                    $dados['ref'] => '/uploads/' . $data['orig_name']
                ));
                $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Foto alterada com sucesso!'));
            } catch (ValidationException $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            } catch (Exception $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            }
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

}
