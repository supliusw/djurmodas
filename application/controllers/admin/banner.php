<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banner extends AdminController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_banner_model', 'bannerModel');
    }

    public function cadastrar() {

        try {
            $config['upload_path'] = APPPATH . '../public_html/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = 'img' . date("YmdHis");

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('path'))
                throw new Exception($this->upload->display_errors());

            $data = $this->upload->data();

            $this->bannerModel->insert(array(
                'path' => '/uploads/' . $data['orig_name']
            ));

            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Img. banner enviada com sucesso!'));
        } catch (ValidationException $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function excluir($id = null) {
        try {
            $this->bannerModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Img. banner excluida com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /*
     * Metodos auxiliares..
     */
}
