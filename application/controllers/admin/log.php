<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_log_model', 'logModel');

    }

    public function index() {

        $this->load->library('filterlist');
        $this->filterlist->setDefaultOrderBy('l.data_hora');
        $this->filterlist->setDefaultAscDesc('DESC');
        $filters = $this->filterlist->getPostFilters(array('like', 'fk_usuario', 'data_inicio', 'data_fim'));

        $this->db->select('l.*, u.usuario');
        $this->db->from('tb_log l');
        $this->db->join('tb_usuario u', 'l.fk_usuario = u.id', 'left');

        /*
         * Filtros..
         */
        if ($filters['like'] != '') {
            $this->db->like('l.antes', $filters['like']);
            $this->db->or_like('l.depois', $filters['like']);
        }

        if ($filters['fk_usuario'] > 0)
            $this->db->where('l.fk_usuario', $filters['fk_usuario']);

        //Data Hora
        if (isDateBR($filters['data_inicio'], true))
            $this->db->where('l.data_hora >=', dateBrToUs($filters['data_inicio'], true, true));
        else
            $filters['data_inicio'] = '';

        if (isDateBR($filters['data_fim'], true))
            $this->db->where('l.data_hora <=', dateBrToUs($filters['data_inicio'], true, true));
        else
            $filters['data_fim'] = '';

        /*
         * Paginação..
         */
        $db = clone $this->db;
        $this->template->set('paginacao', array(
            'limit' => $this->filterlist->getLimit(),
            'offset' => $this->filterlist->getOffset(),
            'qtd' => $db->count_all_results()
        ));

        /*
         * Ordenação..
         */
        $this->db->order_by($this->filterlist->getOrderBy(), $this->filterlist->getAscDesc());
        $this->db->limit($this->filterlist->getLimit(), $this->filterlist->getOffset());

        $this->template->set('ordenacao', array(
            'orderBy' => $this->filterlist->getOrderBy(),
            'ascDesc' => $this->filterlist->getAscDesc()
        ));

        /*
         * Consultando
         */
        $query = $this->db->get();
        $lista = $query->result_array();

        $this->template->set('lista', $lista);
        $this->template->set('filters', $filters);

        $this->template->set('lista', $lista);
        $this->template->set('filters', $filters);


        /*
         * Buscando os tipo existentes
         */
        $this->template->set('tipos', $this->db->select("distinct(l.tabela_label) as tabela_label")
                        ->from('tb_log l')
                        ->order_by('l.tabela_label')
                        ->get()
                        ->result_array()
        );

        /*
         * Buscando os usuarios existentes
         */
        $this->template->set('usuarios', $this->db->select("u.id, u.usuario")
                        ->from('tb_usuario u')
                        ->order_by('u.usuario')
                        ->get()
                        ->result_array()
        );

        $this->template->view('admin/log/index.php');
    }

    public function ver($id = null) {
        try {
            $log = $this->logModel->getCompletById($id);

            $antes = array();
            if (!is_null($log['antes']))
                $antes = json_decode($log['antes'], true);

            $depois = array();
            if (!is_null($log['depois']))
                $depois = json_decode($log['depois'], true);


            if (!is_null($log['tabela'])) {
                $model = ucfirst($log['tabela']) . '_model';
                $this->load->model($model, 'registroAtualModel');
                try {
                    $registroAtual = $this->registroAtualModel->getById($log['chave']);
                    $this->template->set('registroAtual', $registroAtual);

                    $this->load->model('Tb_usuario_model', 'usuarioModel');
                    $usuario = $this->usuarioModel->getById($registroAtual['fk_usuario']);
                    $this->template->set('usuario', $usuario);
                } catch (Exception $e) {
                    
                }
            }

            $this->template->set('antes', $antes);
            $this->template->set('depois', $depois);

            $this->template->set('log', $log);
            $this->template->view('admin/log/ver.php');
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

}
