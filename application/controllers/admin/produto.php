<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produto extends AdminController {

    public function __construct() {
        parent::__construct();

        $this->load->model('Tb_produto_model', 'produtoModel');
        $this->load->model('Tb_produto_foto_model', 'produtoFotoModel');
    }

    public function index() {

        $this->load->library('filterlist');
        $this->filterlist->setDefaultOrderBy('p.id');
        $filters = $this->filterlist->getPostFilters(array('like'));

        $this->db->select("p.*, pf.path")
                ->from('tb_produto p')
                ->join('tb_produto_foto pf', 'p.fk_produto_foto = pf.id', 'left');

        /*
         * Filtros..
         */
        if ($filters['like'] != '') {
            $this->db->like('p.categoria', $filters['like'])
                    ->or_like('p.nome', $filters['like']);
        }

        /*
         * Paginação..
         */
        $db = clone $this->db;
        $this->template->set('paginacao', array(
            'limit' => $this->filterlist->getLimit(),
            'offset' => $this->filterlist->getOffset(),
            'qtd' => $db->count_all_results()
        ));

        /*
         * Ordenação..
         */
        $this->db->order_by($this->filterlist->getOrderBy(), $this->filterlist->getAscDesc());
        $this->db->limit($this->filterlist->getLimit(), $this->filterlist->getOffset());

        $this->template->set('ordenacao', array(
            'orderBy' => $this->filterlist->getOrderBy(),
            'ascDesc' => $this->filterlist->getAscDesc()
        ));

        /*
         * Consultando
         */
        $query = $this->db->get();
        $lista = $query->result_array();

        $this->template->set('lista', $lista);
        $this->template->set('filters', $filters);

        $this->template->view('admin/produto/index.php');
    }

    public function cadastrar() {
        $dados = $this->input->post();

        if ($dados) {
            try {
                $id = $this->produtoModel->insert($dados);
                $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Produto cadastrado com sucesso!'));
                redirect('/admin/produto/ver/' . $id);
            } catch (ValidationException $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getDetailList()));
            } catch (Exception $e) {
                $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            }
            $this->session->set_flashdata('post', $this->input->post());
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->template->view('admin/produto/cadastrar.php');
        }
    }

    public function ver($id = null) {
        try {
            $produto = $this->produtoModel->getCompletById($id);

            //Selecionando todas as Fotos
            $this->template->set('fotos', $this->produtoFotoModel->getByParam(array('fk_produto' => $id)));

            $this->template->set('dados', $produto);
            $this->template->view('admin/produto/ver.php');
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function editar($id = null) {
        try {
            $this->produtoModel->updateById($id, $this->input->post());
            $return = array('code' => 0);
        } catch (ValidationException $e) {
            $return = array('code' => 98, 'message' => $e->getDetailList());
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    public function excluir($id = null) {
        try {
            $this->produtoModel->deleteById($id);
            $this->session->set_flashdata('alert', array('tipo' => 'alert-success', 'mensagem' => 'Produto excluido com sucesso!'));
        } catch (Exception $e) {
            $this->session->set_flashdata('alert', array('tipo' => 'alert-danger', 'mensagem' => $e->getMessage()));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function paginaInicial($id) {
        $post = $this->input->post();

        try {
            if (is_null($id))
                throw new Exception('Nenhum identificador informado');

            $this->produtoModel->updateById($id, array('pagina_inicial' => $post['mostrar'] == 'true' ? 'SIM' : 'NAO'));

            $return = array('code' => 0, 'message' => 'Ok');
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }

        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    public function setParamProduto($id = null) {
        try {
            $this->produtoModel->updateById($id, $this->input->post());
            $return = array('code' => 0);
        } catch (Exception $e) {
            $return = array('code' => 99, 'message' => $e->getMessage());
        }
        $json = json_encode($return);
        $this->output
                ->set_header("Access-Control-Allow-Origin: *")
                ->set_content_type('application/json')
                ->set_output(!is_null($callback) ? "{$callback}($json)" : $json);
    }

    /*
     * Metodos auxiliares..
     */
}
