<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produtos extends SiteController {

    public function __construct() {
        parent::__construct();
        $this->load->model('Tb_produto_foto_model', 'produtoFotoModel');
    }

    public function index() {

        //Produtos
        $result = $this->db->select('pf.path, pf.id as idProdutoFoto, p.*')
                ->from('tb_produto_foto pf')
                ->join('tb_produto p', 'pf.fk_produto = p.id', 'left')
                ->get()
                ->result_array();

        $produtos = array();
        foreach ($result as $row) {
            $produtos[$row['categoria']][] = $row;
        }

        $this->template->set('produtos', $produtos);
        $this->template->view('produtos');
    }
    
    public function detalhe($id = null) {
        $this->template->set('produtoFoto', $this->produtoFotoModel->getCompletById($id));
        $this->template->view('produtosDetalhe');
    }

}
