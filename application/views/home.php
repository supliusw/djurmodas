<div class="row" style="margin-top: 40px;">
    <div class="col-lg-12">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php for ($i = 0; $i < count($banners); $i++) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : '' ?>"></li>
                <?php } ?>
            </ol>
            <div class="carousel-inner">
                <?php for ($i = 0; $i < count($banners); $i++) { ?>
                    <div class="item <?php echo $i == 0 ? 'active' : '' ?>">
                        <img src="<?php echo $banners[$i]; ?>" />
                    </div>
                <?php } ?>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div><!-- /.carousel -->
    </div>
</div>

<div class="row" style="margin-top: 40px;">
    <div class="col-xs-4">
        <a href="/produtos#liganete">
            <img src="<?php echo $config['pg_inicial_liganete']; ?>" width="259" height="540" />     
        </a>
    </div>
    <div class="col-xs-4">
        <a href="/produtos#cotton">
            <img src="<?php echo $config['pg_inicial_coton']; ?>" width="259" height="540"  />   
        </a>
    </div>
    <div class="col-xs-4">
        <img src="<?php echo $config['pg_inicial_outros']; ?>" width="259" height="540"  usemap="#outros" />
        <map name="outros">
            <area shape="rect" coords="0,0,259,258" href="/produtos#camisolas">
        </map>
    </div>
</div>

<!-- Important Owl stylesheet -->
<link rel="stylesheet" href="/css/plugins/owl-carousel/owl.carousel.css">

<!-- Default Theme -->
<link rel="stylesheet" href="/css/plugins/owl-carousel/owl.theme.css">

<script src="/js/plugins/owl-carousel/owl.carousel.js"></script>

<div class="row" style="margin-top: 40px;">
    <h3 class="headDivider"><span>PRODUTOS</span></h3>

    <div id="owl-example" class="owl-carousel owl-theme" style="margin-left:20px; margin-top: 40px;">
        <?php for ($i = 0; $i < count($produtos); $i++) { ?>
            <div>
                <a href="/produtos/detalhe/<?php echo $produtos[$i]['idProdutoFoto']; ?>"><img style="width: 180px; height: 242px;" src="<?php echo $produtos[$i]['path']; ?>" /></a>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    $(document).ready(function() {

        $("#owl-example").owlCarousel({
            navigation: true,
            pagination: false,
            navigationText: [
                "<i class='glyphicon glyphicon-chevron-left'></i>",
                "<i class='glyphicon glyphicon-chevron-right'></i>"
            ]
        });

    });
</script>