<div class="row" style="margin-top: 40px;">
    <div class="col-lg-12">
        <h2><?php echo $produtoFoto['fk_produto']['detail']['nome'] ?></h2>
    </div>
</div>

<hr />

<div class="row" style="margin-top: 10px;">

    <div class="col-xs-8" style="text-align: center;">
        <img src="<?php echo $produtoFoto['path'] ?>" />
    </div>
    <div class="col-xs-4">
        <h4>Detalhes</h4>
        <p>
            <strong>Código:</strong> <?php echo $produtoFoto['fk_produto']['detail']['codigo'] ?>
        </p>
        <p>
            <strong>Composição:</strong> <?php echo $produtoFoto['fk_produto']['detail']['composicao'] ?>
        </p>
        <p>
            <strong>Cores:</strong> <?php echo $produtoFoto['fk_produto']['detail']['cores'] ?>
        </p>
        <p>
            <strong>Grade:</strong> <?php echo $produtoFoto['fk_produto']['detail']['grade'] ?>
        </p>
        <a href="/contato" class="btnSaibaMais">PEDIDOS</a><br /><br />
        <a href="/produtos#<?php echo $produtoFoto['fk_produto']['detail']['categoria'] ?>"><?php echo $produtoFoto['fk_produto']['detail']['categoria'] ?></a>
    </div>

</div>


<script>
    $(document).ready(function() {

    });
</script>