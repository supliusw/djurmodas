<div class="row">
    <div class="col-lg-12">
        <h1>Produtos</h1>
        <hr />
    </div>
</div><!-- /.row -->

<?php if (isset($alert)) { ?>
    <div class="row-fluid">
        <div class="alert span11 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dados principais do Produto:
            </div>
            <div class="panel-body">
                <form role="form" class="formEdit" itemref="admin/produto" itemid="<?php echo $dados['id'] ?>">
                    <div class="form-group">
                        <label>Nome:</label>
                        <input class="form-control" name="nome" value="<?php echo $dados['nome'] ?>">
                    </div>  

                    <div class="form-group">
                        <label>Código:</label>
                        <input class="form-control" name="codigo" value="<?php echo $dados['codigo'] ?>">
                    </div>  

                    <div class="form-group">
                        <label>Composição:</label>
                        <input class="form-control" name="composicao" value="<?php echo $dados['composicao'] ?>">
                    </div>  

                    <div class="form-group">
                        <label>Cores:</label>
                        <input class="form-control" name="cores" value="<?php echo $dados['cores'] ?>">
                    </div>  

                    <div class="form-group">
                        <label>Grade:</label>
                        <input class="form-control" name="grade" value="<?php echo $dados['grade'] ?>">
                    </div>  

                    <div class="form-group">
                        <label>Categoria:</label>
                        <select class="form-control" name="categoria">
                            <option value="outros" <?php echo $dados['categoria'] == 'outros' ? 'selected' : '' ?>>Outros</option>
                            <option value="liganete" <?php echo $dados['categoria'] == 'liganete' ? 'selected' : '' ?>>Liganete</option>
                            <option value="cotton" <?php echo $dados['categoria'] == 'cotton' ? 'selected' : '' ?>>Cotton</option>
                            <option value="pijamas" <?php echo $dados['categoria'] == 'pijamas' ? 'selected' : '' ?>>Camisolas e Pijamas</option>
                            <option value="lingerie" <?php echo $dados['categoria'] == 'lingerie' ? 'selected' : '' ?>>Lingerie</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Página principal:</label><br />
                        <input itemid="<?php echo $dados['id'] ?>" type="radio" data-size="mini" <?php echo ($dados['pagina_inicial'] == "SIM") ? 'checked' : '' ?> class="toggle none paginaInicial" data-on-color="success" data-off-color="default" data-on-text="SIM" data-off-text="NAO" />
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastrar Foto:
            </div>
            <div class="panel-body">
                <form role="form" method="POST" action="/admin/produtoFoto/cadastrar" enctype="multipart/form-data">
                    <input type="hidden" name="fk_produto" value="<?php echo $dados['id'] ?>">

                    <label>Selecione uma foto:</label>
                    <div class="form-group">
                        <input type="file" name="url_foto" />
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Fotos do produto
            </div>
            <div class="panel-body">


                <div class="row">
                    <div class="col-lg-12">
                        <?php if (count($fotos) > 0) { ?>
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th width="140">Principal</th> 
                                        <th>Path</th> 
                                        <th>Largura</th> 
                                        <th>Altura</th> 
                                        <th>Excluir</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($fotos as $row) { ?>
                                        <tr>
                                            <td><input type="radio" class="principal toggle" name="principal" itemid="<?php echo $row['id'] ?>" <?php echo ($row['id'] == $dados['fk_produto_foto']['id']) ? 'checked' : '' ?> data-on-color="success" data-size="mini" data-off-color="danger" data-on-text="Sim" data-off-text="Não" /></td> 
                                            <td>
                                                <a href="<?php echo $row['path'] ?>" target="_blank">
                                                    <img src="<?php echo $row['path'] ?>" width="50" height="80" />
                                                </a>
                                            </td> 
                                            <td><?php echo $row['width'] ?>px</td> 
                                            <td><?php echo $row['height'] ?>px</td> 
                                            <td><a href="/admin/produtoFoto/excluir/<?php echo $row['id'] ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <div class="alert alert-danger">
                                Este produto não possui fotos cadastradas
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $(".principal").on('switchChange', function(e, data) {
            $.post('/admin/produto/setParamProduto/<?php echo $dados['id'] ?>', {fk_produto_foto: data.value ? $(this).attr('itemid') : 'null'});
        });

        $(".paginaInicial").on('switchChange', function(e, data) {
            $.post('/admin/produto/paginaInicial/' + $(this).attr('itemid'), {mostrar: data.value});
        });
    });
</script>