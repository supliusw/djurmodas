<div class="row">
    <div class="col-lg-12">
        <h1>Produtos</h1>
        <hr />
    </div>
</div><!-- /.row -->

<?php if (isset($alert)) { ?>
    <div class="row-fluid">
        <div class="alert span11 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div id="content-container">

    <form id="formFilter" method="post" class="form-horizontal">
        <input type="hidden" value="<?php echo $ordenacao['orderBy']; ?>" id="orderBy" name="orderBy" />
        <input type="hidden" value="<?php echo $ordenacao['ascDesc']; ?>" id="ascDesc" name="ascDesc" />

        <div id="accordion" class="panel-group accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="input-group" style="padding: 10px;">
                            <input class="form-control" type="text" name="filter_like" value="<?php echo $filters['like']; ?>" placeholder="Digite uma palavra chave.." />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Filtrar</button>
                                <button class="btn btn-default " type="button" href="#collapseOne" data-parent=".accordion" data-toggle="collapse">Avançado</button>
                            </span>
                        </div>
                    </h4>
                </div>

                <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                    <div class="panel-body">
                        Nenhum filtro avançado
                    </div>
                </div>
            </div> <!-- /.panel-default -->
        </div>
    </form>

    <br />

    <div class="row">
        <div class="col-xs-6 pull-left">
            <a class="btn btn-sm btn-success" href="/<?php echo $this->uri->segment(1); ?>/produto/cadastrar"><i class="fa fa-plus-circle"></i> Criar novo Produto</a>
        </div>
        <div class="col-xs-6 pull-right">
            <div class="input-group input-group-sm">
                <span class="input-group-addon">Mostrar</span>
                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $paginacao['limit']; ?>">
                <span class="input-group-addon">a partir do</span>
                <input type="text" class="form-control" id="offset" name="offset" value="<?php echo $paginacao['offset']; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="$('#formFilter').submit();">Filtrar</button>
                </span>
            </div>
        </div>
    </div>

    <hr />

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="orderBy">
                    <th>Foto</th>
                    <th itemid="p.categoria">Categoria<i class="caret"></i></th>
                    <th itemid="p.nome">Nome<i class="caret"></i></th>
                    <th itemid="p.codigo">Código<i class="caret"></i></th>
                    <th itemid="p.composicao">Composição<i class="caret"></i></th>
                    <th itemid="p.cores">Cores<i class="caret"></i></th>
                    <th itemid="p.grade">Grade<i class="caret"></i></th>
                    <th>Pagina inicial</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($lista) > 0) { ?>
                    <?php foreach ($lista as $i) { ?>
                        <tr>
                            <td>
                                <?php if ($i['fk_produto_foto'] > 0) { ?>
                                    <a href="<?php echo $i['path'] ?>" target="_blank">
                                        <img src="<?php echo $i['path'] ?>" width="50" height="90" />
                                    </a>
                                <?php } else { ?>
                                    -
                                <?php } ?>
                            </td>
                            <td>
                                <?php echo format_destacar(ucfirst($i['categoria']), $filters['like']) ?>
                            </td>
                            <td>
                                <?php echo format_destacar($i['nome'], $filters['like']) ?>
                            </td>
                            
                            <td>
                                <?php echo format_destacar($i['codigo'], $filters['like']) ?>
                            </td>
                            
                            <td>
                                <?php echo format_destacar($i['composicao'], $filters['like']) ?>
                            </td>
                            
                            <td>
                                <?php echo format_destacar($i['cores'], $filters['like']) ?>
                            </td>
                            
                            <td>
                                <?php echo format_destacar($i['grade'], $filters['like']) ?>
                            </td>
                            <td>
                                <input type="radio"  <?php echo $i['fk_produto_foto'] > 0 ? '' : 'disabled' ?> class="paginaInicial toggle" itemid="<?php echo $i['id'] ?>" <?php echo ($i['pagina_inicial'] == 'SIM') ? 'checked' : '' ?> data-on-color="success" data-size="mini" data-off-color="danger" data-on-text="SIM" data-off-text="NAO" />
                            </td> 
                            <td>
                                <a href="/admin/produto/ver/<?php echo $i['id'] ?>" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></a>
                                <a href="/admin/produto/excluir/<?php echo $i['id'] ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="5">Nenhum registro encontrado</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- /.table-responsive -->

    <?php paginacao($paginacao); ?>





</div> <!-- /#content-container -->


<script type="text/javascript">
    $(function() {
        $("#formFilter").submit(function() {
            var limit = $("#limit").clone();
            limit.css('display', 'none');

            var offset = $("#offset").clone();
            offset.css('display', 'none');

            $(this).append(limit);
            $(this).append(offset);
            return true;
        });


        $(".paginaInicial").on('switchChange', function(e, data) {
            $.post('/admin/produto/paginaInicial/' + $(this).attr('itemid'), {mostrar: data.value});
        });

    });

</script>