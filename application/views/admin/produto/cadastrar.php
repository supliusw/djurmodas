<div class="row">
    <div class="col-lg-12">
        <h1>Produtos</h1>
        <hr />
    </div>
</div><!-- /.row -->

<?php if (isset($alert)) { ?>
    <div class="row-fluid">
        <div class="alert span11 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dados do produto:
            </div>
            <div class="panel-body">
                <form role="form" method="POST">
                    
                    <div class="form-group">
                        <label>Nome:</label>
                        <input class="form-control" name="nome" value="<?php echo $post['nome'] ?>">
                    </div>  
                    
                    <div class="form-group">
                        <label>Categoria:</label>
                        <select class="form-control" name="categoria">
                            <option value="outros" <?php echo $post['categoria']=='outros'?'selected':'' ?>>Outros</option>
                            <option value="liganete" <?php echo $post['categoria']=='liganete'?'selected':'' ?>>Liganete</option>
                            <option value="cotton" <?php echo $post['categoria']=='cotton'?'selected':'' ?>>Cotton</option>
                            <option value="pijamas" <?php echo $post['categoria']=='pijamas'?'selected':'' ?>>Camisolas e Pijamas</option>
                            <option value="lingerie" <?php echo $post['categoria'] == 'lingerie' ? 'selected' : '' ?>>Lingerie</option>
                        </select>
                    </div>   
                    
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <button type="reset" class="btn btn-danger">Limpar</button>
                </form>
                
                <div class="row" style="padding-left: 15px; padding-right: 15px; margin-top: 20px;">
                    <div class="alert col-lg-12 alert-warning">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Atenção: Informações adicionais serão cadastradas na próxima tela
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    $(function() {
    });
</script>