<br />

<ol class="breadcrumb">
    <li><a href="#">Configurações</a></li>
    <li><a href="/meusDados">Meus Dados</a></li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dados principais:
            </div>
            <div class="panel-body">
                <?php $this->template->view('common/pessoa/formEditPessoa.php', false); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">


        <!-- Nav tabs -->
        <ul class="nav nav-tabs keep">
            <li class="active">
                <a href="#usuarios" data-toggle="tab">Usuários</a>
            </li>
            <li>
                <a href="#enderecos" data-toggle="tab">Endereços</a>
            </li>
            <li>
                <a href="#contatos" data-toggle="tab">Contatos</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <!-- Usuários -->
            <div class="tab-pane active" id="usuarios">
                <?php $this->template->view('common/usuario/formListPessoa.php', false); ?>
            </div>

            <!-- Endereços -->
            <div class="tab-pane " id="enderecos">
                <?php $this->template->view('common/endereco/formListPessoa.php', false); ?>
            </div>

            <!-- Contatos -->
            <div class="tab-pane fade" id="contatos">
                <?php $this->template->view('common/contato/formListPessoa.php', false); ?>
            </div>

        </div>
    </div>
</div>


<script>
    $(function() {

    });
</script>