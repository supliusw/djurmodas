<br />

<ol class="breadcrumb">
    <li><a href="#">Configurações</a></li>
    <li><a href="/log">Logs</a></li>
    <li class="active">Detalhe</li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Detalhes do log:
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Evento:</label><h3 style="margin: 0px;">
                        <?php
                        if (!is_null($log['antes']) && !is_null($log['depois'])) {
                            echo '<span class="label label-info">Alteração</span>';
                        } else {
                            if (is_null($log['antes']) && !is_null($log['depois'])) {
                                echo '<span class="label label-success">Inclusão</span>';
                            } else {
                                if (!is_null($log['antes']) && is_null($log['depois'])) {
                                    echo '<span class="label label-danger">Exclusão</span>';
                                } else {
                                    echo '<span class="label label-default">Outros</span>';
                                }
                            }
                        }
                        ?>
                    </h3>
                </div>
                <div class="form-group">
                    <label>Data/Hora:</label><br />
                    <?php echo dateUsToBr($log['data_hora'], true, true) ?>
                </div>     
                <div class="form-group">
                    <label>Usuário:</label><br />
                    <?php echo $usuario['usuario'] ?>
                </div>   
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Campos alterados:
            </div>
            <div class="panel-body">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Campo</th>
                            <th>Antes</th>
                            <th>Depois</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($antes as $k => $v) { ?>
                            <?php
                            if (array_key_exists($k, $depois)) {
                                $d = $depois[$k];
                                unset($depois[$k]);
                            }
                            ?>
                            <tr>
                                <td><?php echo $k ?></td> 
                                <td><?php echo is_null($v) ? '-' : $v; ?></td> 
                                <td><?php echo is_null($d) ? '-' : $d; ?></td> 
                            </tr>
                        <?php } ?>
                        <?php foreach ($depois as $k => $v) { ?>
                            <tr>
                                <td><?php echo $k ?></td> 
                                <td>-</td> 
                                <td><?php echo is_null($v) ? '-' : $v; ?></td> 
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Registro atualmente:
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Tipo:</label><br />
                            <?php echo $log['tabela_label'] ?>
                        </div>    
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Chave:</label><br />
                            <?php echo $log['chave'] ?>
                        </div> 
                    </div>
                </div>
                <?php if (is_null($registroAtual)) { ?>
                    <div class="row" style="padding-left: 15px; padding-right: 15px;">
                        <div class="alert col-lg-12 alert-danger">
                            Este registro não existe mais
                        </div>
                    </div>
                <?php } else { ?>
                    <hr />
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Campo</th>
                                <th>Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($registroAtual as $k => $v) { ?>
                                <tr>
                                    <td><?php echo $k ?></td> 
                                    <td><?php echo $v; ?></td> 
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {

    }
</script>