<br />

<ol class="breadcrumb">
    <li><a href="#">Configurações</a></li>
    <li class="active">Log</li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div id="content-container">

    <form id="formFilter" method="post" class="form-horizontal">
        <input type="hidden" value="<?php echo $ordenacao['orderBy']; ?>" id="orderBy" name="orderBy" />
        <input type="hidden" value="<?php echo $ordenacao['ascDesc']; ?>" id="ascDesc" name="ascDesc" />

        <div id="accordion" class="panel-group accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="input-group" style="padding: 10px;">
                            <input class="form-control" type="text" name="filter_like" value="<?php echo $filters['like']; ?>" placeholder="Digite uma palavra chave.." />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Filtrar</button>
                                <button class="btn btn-default " type="button" href="#collapseOne" data-parent=".accordion" data-toggle="collapse">Avançado</button>
                            </span>
                        </div>
                    </h4>
                </div>

                <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="col-lg-4">
                                <label>Tipo de registro:</label>
                                <select class="form-control input-sm" name="filter_tipo_registro">
                                    <option value="">Todos</option>
                                    <?php if (count($tipos) > 0) { ?>
                                        <?php foreach ($tipos as $row) { ?>
                                            <option value="<?php echo $row['tabela_label'] ?>" <?php echo $row['tabela_label'] == $filters['tipo_registro'] ? 'selected' : '' ?>><?php echo $row['tabela_label'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label>Usuário:</label>
                                <select class="form-control input-sm" name="filter_fk_usuario">
                                    <option value="">Todos</option>
                                    <?php if (count($usuarios) > 0) { ?>
                                        <?php foreach ($usuarios as $row) { ?>
                                            <option value="<?php echo $row['id'] ?>" <?php echo $row['id'] == $filters['fk_usuario'] ? 'selected' : '' ?>><?php echo $row['usuario'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label>Período:</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control dateTimePicker" data-date-format="DD/MM/YYYY HH:mm" name="filter_data_inicio" placeholder="De"  value="<?php echo $filters['data_inicio']; ?>">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control dateTimePicker" data-date-format="DD/MM/YYYY HH:mm" name="filter_data_fim" placeholder="Até" value="<?php echo $filters['data_fim']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /.panel-default -->
        </div>
    </form>

    <br />

    <div class="row">
        <div class="col-xs-6 pull-left">
        </div>
        <div class="col-xs-6 pull-right">
            <div class="input-group input-group-sm">
                <span class="input-group-addon">Mostrar</span>
                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $paginacao['limit']; ?>">
                <span class="input-group-addon">a partir do</span>
                <input type="text" class="form-control" id="offset" name="offset" value="<?php echo $paginacao['offset']; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="$('#formFilter').submit();">Filtrar</button>
                </span>
            </div>
        </div>
    </div>

    <hr />

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="orderBy">
                    <th itemid="l.tabela_label">Tipo de registro<i class="caret"></i></th>
                    <th itemid="l.chave">Código<i class="caret"></i></th>
                    <th>Antes</th>
                    <th>Depois</th>
                    <th itemid="usuario">Usuário<i class="caret"></i></th>
                    <th itemid="l.data_hora">Data/Hora<i class="caret"></i></th>
                    <th width="20">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php

                function format($json) {
                    $return = $json;
                    $array = json_decode($json, true);
                    if ($array && count($array) > 0) {
                        $return = "";
                        foreach ($array as $key => $value) {
                            $return .= '<strong>' . ucfirst($key) . ':</strong>&nbsp;' . strtoupper($value) . ', ';
                        }
                        $return = rtrim($return, ", ");
                    }
                    return $return;
                }
                ?>


                <?php if (count($lista) > 0) { ?>
                    <?php foreach ($lista as $i) { ?>

                        <?php
                        if (!is_null($i['antes']) && !is_null($i['depois'])) {
                            $class = 'label-info';
                        } else {
                            if (is_null($i['antes']) && !is_null($i['depois'])) {
                                $class = 'label-success';
                            } else {
                                if (!is_null($i['antes']) && is_null($i['depois'])) {
                                    $class = 'label-danger';
                                } else {
                                    $class = 'label-default';
                                }
                            }
                        }
                        ?>

                        <?php $i['antes'] = format($i['antes']); ?>
                        <?php $i['depois'] = format($i['depois']); ?>

                        <tr>
                            <td><span class="label <?php echo $class ?>"><?php echo $i['tabela_label'] ?></span></td>
                            <td><?php echo format_destacar($i['chave'], $filters['like']) ?></td>
                            <?php if (!is_null($i['antes'])) { ?>
                                <td style="padding: 4px;"><pre style="margin: 0px; padding: 4px; width: 300px;"><?php echo format_destacar($i['antes'], $filters['like']) ?></pre></td>
                            <?php } else { ?>
                                <td>-</td>
                            <?php } ?>
                            <?php if (!is_null($i['depois'])) { ?>
                                <td style="padding: 4px;"><pre style="margin: 0px; padding: 4px; width: 300px;"><?php echo format_destacar($i['depois'], $filters['like']) ?></pre></td>
                            <?php } else { ?>
                                <td>-</td>
                            <?php } ?>
                            <td><?php echo $i['usuario'] ?></td>
                            <td><?php echo dateUsToBr($i['data_hora'], true, true) ?></td>
                            <td><a href="/admin/log/ver/<?php echo $i['id'] ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a> </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="5">Nenhum registro encontrado</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- /.table-responsive -->

    <?php paginacao($paginacao); ?>





</div> <!-- /#content-container -->

<script src="/js/plugins/jquery.nicescroll.min.js"></script>
<script type="text/javascript">
                        $(function() {
                            $("pre").niceScroll();

                            $("#formFilter").submit(function() {
                                var limit = $("#limit").clone();
                                limit.css('display', 'none');

                                var offset = $("#offset").clone();
                                offset.css('display', 'none');

                                $(this).append(limit);
                                $(this).append(offset);
                                return true;
                            });

                        });
</script>