<ul class="nav navbar-nav navbar-right navbar-user">
    
    <li>
        <a href="/"><i class="fa fa-globe"></i> Ir para o site</a>
    </li>
    
    <li class="dropdown user-dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $ADMIN['fk_pessoa']['detail']['nome'] ?> <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="/admin/myself"><i class="fa fa-user"></i> <?php echo $usuario['usuario'] ?></a></li>
            <li class="divider"></li>
            <?php if (count($this->usuarioPerfis) > 1) { ?>
                <li><a href="/common/login/escolherPerfil"><i class="fa fa-users"></i> Trocar perfil</a></li>
            <?php } ?>
            <li><a href="/common/login/logout"><i class="fa fa-power-off"></i> Sair</a></li>
        </ul>
    </li>
</ul>