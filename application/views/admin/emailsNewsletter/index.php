<div class="row">
    <div class="col-lg-12">
        <h1>Cadastro para enviar Newsletter</h1>
        <hr />
    </div>
</div><!-- /.row -->

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div id="content-container">

    <form id="formFilter" method="post" class="form-horizontal">
        <input type="hidden" value="<?php echo $ordenacao['orderBy']; ?>" id="orderBy" name="orderBy" />
        <input type="hidden" value="<?php echo $ordenacao['ascDesc']; ?>" id="ascDesc" name="ascDesc" />

        <div id="accordion" class="panel-group accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="input-group" style="padding: 10px;">
                            <input class="form-control" type="text" name="filter_like" value="<?php echo $filters['like']; ?>" placeholder="Digite uma palavra chave.." />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Filtrar</button>
                                <button class="btn btn-default " type="button" href="#collapseOne" data-parent=".accordion" data-toggle="collapse">Avançado</button>
                            </span>
                        </div>
                    </h4>
                </div>

                <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                    <div class="panel-body">
                        Nenhum filtro avançado
                    </div>
                </div>
            </div> <!-- /.panel-default -->
        </div>
    </form>

    <br />

    <div class="row">
        <div class="col-xs-6 pull-left">
        </div>
        <div class="col-xs-6 pull-right">
            <div class="input-group input-group-sm">
                <span class="input-group-addon">Mostrar</span>
                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $paginacao['limit']; ?>">
                <span class="input-group-addon">a partir do</span>
                <input type="text" class="form-control" id="offset" name="offset" value="<?php echo $paginacao['offset']; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="$('#formFilter').submit();">Filtrar</button>
                </span>
            </div>
        </div>
    </div>

    <hr />

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="orderBy">
                    <th itemid="e.email">Email<i class="caret"></i></th>
                </tr>
            </thead>
            <tbody>

                <?php if (count($lista) > 0) { ?>
                    <?php foreach ($lista as $i) { ?>
                        <tr>
                            <td><?php echo format_destacar($i['email'], $filters['like']) ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="5">Nenhum registro encontrado</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- /.table-responsive -->

    <?php paginacao($paginacao); ?>
</div> <!-- /#content-container -->
