<?php if (isset($alert)) { ?>
    <div class="row-fluid">
        <div class="alert span11 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <h1>A empresa</h1>
        <hr />
    </div>
</div><!-- /.row -->


<form role="form" class="formEdit" itemref="admin/home" itemid="<?php echo $config['id'] ?>">
    <div class="row">
        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-info-circle"></i> Texto (Quem somos)</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <textarea name="quem_somos" id="quem_somos"><?php echo $config['quem_somos'] ?></textarea>
                    </div> 
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-info-circle"></i> Texto (Missão)</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <textarea name="missao" id="missao"><?php echo $config['missao'] ?></textarea>
                    </div> 
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-info-circle"></i> Texto (Objetivo)</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <textarea name="objetivo" id="objetivo"><?php echo $config['objetivo'] ?></textarea>
                    </div> 
                </div>
            </div>
        </div>

    </div><!-- /.row -->
</form>

<script>
    $(function() {
        $('#quem_somos').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            onblur: function(e) {
                $('#quem_somos').html($(this).html());
                changeFields($('#quem_somos'));
            }
        });

        $('#missao').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            onblur: function(e) {
                $('#missao').html($(this).html());
                changeFields($('#missao'));
            }
        });

        $('#objetivo').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            onblur: function(e) {
                $('#objetivo').html($(this).html());
                changeFields($('#objetivo'));
            }
        });
    });
</script>