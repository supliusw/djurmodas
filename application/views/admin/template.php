<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Dashboard - Djur Admin</title>

        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.css" rel="stylesheet">

        <!-- Add custom CSS here -->
        <link href="/css/sb-admin.css" rel="stylesheet">
        <link rel="stylesheet" href="/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/style.css">

        <!-- Page-Level Plugin CSS -->
        <link rel="stylesheet" href="/css/plugins/datepicker.css">
        <link rel="stylesheet" href="/css/plugins/bootstrap-switch.min.css">
        <link rel="stylesheet" href="/css/plugins/select2.css">
        <link rel="stylesheet" href="/css/plugins/bootstrap-datetimepicker.min">

        <!-- JavaScript -->
        <script src="/js/jquery-1.10.2.js"></script>
        <script src="/js/bootstrap.js"></script>

        <script src="/js/plugins/jquery.mask.min.js"></script>
        <script src="/js/plugins/moment.min.js"></script> 
        <script src="/js/plugins/jquery.redirect.min.js"></script>    
        <script src="/js/plugins/bootstrap-datepicker.js"></script>  
        <script src="/js/plugins/bootstrap-datetimepicker.min.js"></script> 
        <script src="/js/plugins/bootstrap-switch.min.js"></script>
        <script src="/js/plugins/jquery.maskMoney.js"></script>
        <script src="/js/plugins/select2.min.js"></script>
        <script src="/js/plugins/jquery.numeric.js"></script>

        <script src="/js/script.js"></script>
        <link rel="stylesheet" href="/css/summernote-bs3">
        <script src="/js/summernote.js"></script>



    </head>

    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">D'jur Admin</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <?php $this->load->view('admin/menu_left'); ?>
                    <?php $this->load->view('admin/menu_top'); ?>
                </div><!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <?php echo $contents; ?>

            </div><!-- /#page-wrapper -->

        </div><!-- /#wrapper -->





    </body>
</html>
