<?php if (isset($alert)) { ?>
    <div class="row-fluid">
        <div class="alert span11 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <h1>Home</h1>
        <hr />
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Bem vindo ao administrador do seu website! Aqui você poderá gerenciar cadastros de produtos, banners, contatos e outras configurações do seu site.
        </div>
    </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-picture-o"></i> Banners da página inicial</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-warning">
                            Dimensões das imagens: 970x464 pixels
                        </div>
                    </div>
                </div>


                <form role="form" method="POST" action="/admin/banner/cadastrar" enctype="multipart/form-data">
                    <label>Selecione uma foto:</label>
                    <div class="form-group">
                        <input type="file" name="path" />
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                </form>

                <hr />

                <div class="row">
                    <div class="col-lg-12">
                        <?php if (count($banners) > 0) { ?>
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th>Imagem</th> 
                                        <th>Excluir</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($banners as $row) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo $row['path'] ?>">
                                                    <img src="<?php echo $row['path'] ?>" width="200" height="100" />
                                                </a>
                                            </td> 
                                            <td><a href="/admin/banner/excluir/<?php echo $row['id'] ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <div class="alert alert-danger">
                                Nenhuma imagem foi enviada para aparecer no banner
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>        

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-map-marker"></i> Mapa/Contato</h3>
            </div>
            <div class="panel-body">
                <form role="form" class="formEdit" itemref="admin/home" itemid="<?php echo $config['id'] ?>">
                    <div class="form-group">
                        <label>Latitude:</label>
                        <input class="form-control" name="latitude" value="<?php echo $config['latitude'] ?>">
                    </div>  
                    <div class="form-group">
                        <label>Longitude:</label>
                        <input class="form-control" name="longitude" value="<?php echo $config['longitude'] ?>">
                    </div>  
                    <div class="form-group">
                        <label>Localização:</label>
                        <textarea class="form-control" name="localizacao" id="localizacao"><?php echo $config['localizacao'] ?></textarea>
                    </div> 
                </form>
            </div>
        </div>

    </div>

    <div class="col-lg-6">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-picture-o"></i> Imagens dos links da pagina inicial</h3>
            </div>
            <div class="panel-body">
                <form role="form" method="POST" action="/admin/home/alterarFoto" enctype="multipart/form-data">
                    <input type="hidden" name="ref" id="ref" />
                    <input type="file" style="display: none;" name="path" id="path" />
                </form>

                <div class="row">
                    <div class="col-lg-4">
                        <img src="<?php echo $config['pg_inicial_liganete']; ?>" width="129" height="270" />                        
                        <p>
                            <button type="button" itemref="pg_inicial_liganete" class="btn btn-sm btn-primary alterarImagem"><i class="fa fa-upload"></i> Alterar imagem</button>                       
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <img src="<?php echo $config['pg_inicial_coton']; ?>" width="129" height="270" />   
                        <p>
                            <button type="button" itemref="pg_inicial_coton" class="btn btn-sm btn-primary alterarImagem"><i class="fa fa-upload"></i> Alterar imagem</button>                       
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <img src="<?php echo $config['pg_inicial_outros']; ?>" width="129" height="270" />     
                        <p>
                            <button type="button" itemref="pg_inicial_outros" class="btn btn-sm btn-primary alterarImagem"><i class="fa fa-upload"></i> Alterar imagem</button>                       
                        </p>
                    </div>
                </div>
            </div>
        </div>



        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-dashboard"></i> Configurações do site</h3>
            </div>
            <div class="panel-body">
                <form role="form" class="formEdit" itemref="admin/home" itemid="<?php echo $config['id'] ?>">
                    <div class="form-group">
                        <label>Fone:</label>
                        <input class="form-control" name="fones" value="<?php echo $config['fones'] ?>">
                    </div>      
                    <div class="form-group">
                        <label>Facebook:</label>
                        <input class="form-control" name="facebook" value="<?php echo $config['facebook'] ?>">
                    </div>      
                    <div class="form-group">
                        <label>Email:</label>
                        <input class="form-control" name="email" value="<?php echo $config['email'] ?>">
                    </div>      
                    <div class="form-group">
                        <label>Rodapé:</label>
                        <textarea class="form-control" name="rodape" id="rodape"><?php echo $config['rodape'] ?></textarea>
                    </div> 
                </form>
            </div>
        </div>
    </div>

</div><!-- /.row -->

<script>
    $(function() {

        $(".alterarImagem").click(function() {
            var ref = $(this).attr('itemref');
            $("#ref").val(ref);
            $("#path").trigger('click');
        });

        $("#path").change(function() {
            var fileName = $(this).val();
            $(this).parent().submit();
        });

        $('#rodape').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            onblur: function(e) {
                $('#rodape').html($(this).html());
                changeFields($('#rodape'));
            }
        });


        $('#localizacao').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
            ],
            onblur: function(e) {
                $('#localizacao').html($(this).html());
                changeFields($('#localizacao'));
            }
        });


    });
</script>