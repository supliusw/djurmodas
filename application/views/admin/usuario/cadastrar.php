<br />

<ol class="breadcrumb">
    <li><a href="#">Configurações</a></li>
    <li><a href="/usuario">Usuários do sistema</a></li>
    <li class="active">Cadastrar</li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dados do usuario:
            </div>
            <div class="panel-body">
                <form role="form" method="POST">
                    
                    <div class="form-group">
                        <label>Usuário:</label>
                        <input class="form-control" name="usuario" value="<?php echo $post['usuario'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Senha:</label>
                        <input class="form-control" name="senha" />
                    </div>      
                    
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <button type="reset" class="btn btn-danger">Limpar</button>
                </form>
                
                <div class="row" style="padding-left: 15px; padding-right: 15px; margin-top: 20px;">
                    <div class="alert col-lg-12 alert-warning">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Atenção: Informações adicionais serão cadastradas na próxima tela
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    $(function() {
    });
</script>