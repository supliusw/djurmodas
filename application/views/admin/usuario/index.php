<br />

<ol class="breadcrumb">
    <li><a href="#">Usuários do sistema</a></li>
    <li class="active">Lista</li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div id="content-container">

    <form id="formFilter" method="post" class="form-horizontal">
        <input type="hidden" value="<?php echo $ordenacao['orderBy']; ?>" id="orderBy" name="orderBy" />
        <input type="hidden" value="<?php echo $ordenacao['ascDesc']; ?>" id="ascDesc" name="ascDesc" />

        <div id="accordion" class="panel-group accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="input-group" style="padding: 10px;">
                            <input class="form-control" type="text" name="filter_like" value="<?php echo $filters['like']; ?>" placeholder="Digite uma palavra chave.." />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Filtrar</button>
                                <button class="btn btn-default " type="button" href="#collapseOne" data-parent=".accordion" data-toggle="collapse">Avançado</button>
                            </span>
                        </div>
                    </h4>
                </div>

                <div class="panel-collapse collapse" id="collapseOne" style="height: 0px;">
                    <div class="panel-body">
                        Nenhum filtro avançado
                    </div>
                </div>
            </div> <!-- /.panel-default -->
        </div>
    </form>

    <br />

    <div class="row">
        <div class="col-xs-6 pull-left">
            <a class="btn btn-sm btn-success" href="/admin/usuario/cadastrar"><i class="fa fa-plus-circle"></i> Criar usuário</a>
        </div>
        <div class="col-xs-6 pull-right">
            <div class="input-group input-group-sm">
                <span class="input-group-addon">Mostrar</span>
                <input type="text" class="form-control" id="limit" name="limit" value="<?php echo $paginacao['limit']; ?>">
                <span class="input-group-addon">a partir do</span>
                <input type="text" class="form-control" id="offset" name="offset" value="<?php echo $paginacao['offset']; ?>">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" onclick="$('#formFilter').submit();">Filtrar</button>
                </span>
            </div>
        </div>
    </div>

    <hr />

    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="orderBy">
                    <th itemid="u.id">Código<i class="caret"></i></th>
                    <th itemid="u.usuario">Usuário<i class="caret"></i></th>
                    <th itemid="qtd">Qtd. Perfis<i class="caret"></i></th>
                    <th width="20">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($lista) > 0) { ?>
                    <?php foreach ($lista as $i) { ?>
                        <tr>
                            <td><a href="/admin/usuario/ver/<?php echo $i['id'] ?>"><?php echo str_pad($i['id'], 4, '0', STR_PAD_LEFT); ?></a></td>
                            <td><?php format_destacar($i['usuario'], $filters['like']); ?></td>
                            <td><?php echo $i['qtd']; ?></td>
                            <td><a href="/admin/usuario/excluir/<?php echo $i['id'] ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="3">Nenhum registro encontrado</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div> <!-- /.table-responsive -->

    <?php paginacao($paginacao); ?>





</div> <!-- /#content-container -->


<script type="text/javascript">
    $(function() {
        $("#formFilter").submit(function() {
            var limit = $("#limit").clone();
            limit.css('display', 'none');

            var offset = $("#offset").clone();
            offset.css('display', 'none');

            $(this).append(limit);
            $(this).append(offset);
            return true;
        });

        $(".statusLogon").on('switchChange', function(e, data) {
            $.post('/logon/alterarStatus/' + $(this).attr('itemid'), {ativo: data.value});
        });
    });
</script>