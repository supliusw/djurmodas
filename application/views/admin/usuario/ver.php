<br />

<ol class="breadcrumb">
    <li><a href="#">Configurações</a></li>
    <li><a href="#">Usuário</a></li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>


<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dados do usuário:
            </div>
            <div class="panel-body">
                <form role="form" class="formEdit" itemref="admin/usuario" itemid="<?php echo $dados['id'] ?>">
                    <div class="form-group">
                        <label>Usuário:</label>
                        <input class="form-control" name="usuario" value="<?php echo $dados['usuario'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Nova Senha:</label>
                        <input class="form-control" name="senha" />
                    </div>                   
                </form>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Meus perfis:
            </div>
            <div class="panel-body">


                <form id="frmAddPerfil" role="form" method="POST" action="/admin/usuario/addPerfil">
                    <input type="hidden" value="<?php echo $dados['id'] ?>" name="fk_usuario" />
                    <div class="form-group">
                        <div class="controls form-inline">
                            <input type="hidden" name="perfil" id="tipo" value="" />
                            <select class="form-control" id="tipo_id" name="">
                                <?php foreach ($perfis as $key => $value) { ?>
                                    <optgroup label="<?php echo $key ?>" itemref="<?php echo $value['fk'] ?>">
                                        <?php foreach ($value['values'] as $row) { ?>
                                            <option value="<?php echo $row['id'] ?>"><?php echo $row['nome'] ?></option>
                                        <?php } ?>
                                    </optgroup>
                                <?php } ?>
                            </select>
                            <button type="button" class="btn btn-primary" id="btAddPerfil">Adicionar</button>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="40">Perfil</th>
                            <th>Nome</th>
                            <th width="20">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($perfisDoUsuario) > 0) { ?>
                            <?php foreach ($perfisDoUsuario as $row) { ?>
                                <tr>
                                    <td><?php echo $row['perfil'] ?></td>
                                    <td><?php echo $row['fk_pessoa']['detail']['nome'] ?></td>
                                    <td><a href="/admin/usuario/removerPerfil/<?php echo $row['id'] ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<script>
    $(function() {
        $("#btAddPerfil").click(function() {
            var label = $('#tipo_id :selected').parent().attr('label');
            var fk = $('#tipo_id :selected').parent().attr('itemref');

            $("#tipo").val(label);
            $("#tipo_id").attr('name', fk);
            
            $("#frmAddPerfil").submit();
        });
    });
</script>