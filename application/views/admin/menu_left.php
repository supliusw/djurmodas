<?php $controller = $this->router->class; ?>
<?php $action = $this->router->method; ?>

<ul class="nav navbar-nav side-nav">

    <li><a href="/admin/home"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="/admin/empresa"><i class="fa fa-list"></i> A empresa</a></li>
    <li><a href="/admin/produto"><i class="fa fa-bookmark"></i> Produtos</a></li>
    <li><a href="/admin/emailsNewsletter"><i class="fa fa-envelope"></i> Cadastro p/ Newsletter</a></li>


    <li class="dropdown <?php echo ($controller == 'meusDados' || $controller == 'grupoPermissao' || $controller == 'log') ? 'active open' : '' ?>" >
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i> Configurações <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="/admin/meusDados">Meus dados</a></li>
            <li><a href="/admin/usuario">Usuários do sistema</a></li>
        </ul>
    </li>

</ul>