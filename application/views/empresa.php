<div class="row" style="margin-top: 40px;">
    <div class="col-lg-8">
        <img src="/img/empresa.png" style="width: 100%;" />
    </div>
    <div class="col-lg-4">
        <h2>Quem somos</h2>
        <?php echo $quemSomos; ?>
    </div>
</div>



<div class="row" style="margin-top: 40px;">
    <h3 class="headDivider"><span>NOSSA HISTÓRIA</span></h3>

    <div class="col-lg-6 text-center">
        <img src="/img/img2.png" id="imgMissao" />
        <img src="/img/img1.jpg" style="display: none;" id="imgObjetivo" />
    </div>
    <div class="col-lg-6 missao">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a id="aMissao" href="#missao" role="tab" data-toggle="tab">Missão</a></li>
            <li><a id="aObjetivo"  href="#objetivo" role="tab" data-toggle="tab">Objetivo</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="missao">
                <?php echo $missao; ?>
            </div>
            <div class="tab-pane" id="objetivo">
                <?php echo $objetivo; ?>
            </div>
        </div>
    </div>

</div>

<script>
    $(function() {

        $("#aMissao").click(function() {
            $("#imgObjetivo").css('display', 'none');
            $("#imgMissao").css('display', '');
        });
        $("#aObjetivo").click(function() {
            $("#imgMissao").css('display', 'none');
            $("#imgObjetivo").css('display', '');
        });

    });
</script>