<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>D'Jur Modas</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/sb-admin.css">
        <link rel="stylesheet" href="/css/style.css">
        <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'>

        <!-- JavaScript -->
        <script src="/js/jquery-1.10.2.js"></script>
        <script src="/js/bootstrap.js"></script>
    </head>

    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><?php echo $fones; ?></a>
                </div>
                <div class="collapse navbar-collapse pull-right">
                    <ul class="nav navbar-nav">
                        <li><a style="padding: 3px;" href="<?php echo $facebook; ?>"><img src="/img/facebook.png" width="40" height="40"></a></li>
                        <li><a style="padding: 3px;" href="mailto:<?php echo $email; ?>"><img src="/img/email.png" width="40" height="40"></a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container">
            <?php if (isset($alert)) { ?>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-xs-12">
                        <div class="alert <?php echo $alert['tipo']; ?>" role="alert"><?php echo $alert['mensagem']; ?></div>
                    </div>
                </div>
            <?php } ?>

            <div class="row" style="margin-top: 20px;">
                <div class="col-lg-12 text-center">
                    <img src="/img/logo.jpg" />
                </div>
            </div>

            <div class="row" style="margin-top: 20px;">
                <div class="col-lg-12 text-center menuLinks">
                    <a href="/">HOME</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/empresa">A EMPRESA</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/produtos">PRODUTOS</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/contato">CONTATO</a>
                </div>
            </div>


            <?php echo $contents; ?>

        </div><!-- /.container -->


        <div id="footer">
            <div class="container text-center">
                <?php echo $rodape; ?>
            </div>
        </div>

    </body>
</html>
