<div class="row" style="margin-top: 40px;">
    <div id="single_map_canvas" style="width: 100%; height: 450px; position:absolute; left:0; right:0;  overflow: hidden;"></div>
</div>

<div class="row" style="margin-top: 490px;">
    <div class="col-xs-6">
        <h4 class="headDivider"><span style="padding-left: 0px; color:#333;">CONTATO</span></h4>


        <form role="form" action="/contato/enviar" method="POST">
            <div class="form-group">
                <label for="nome">NOME</label>
                <input type="nome" class="form-control" id="nome" name="nome" />
            </div>
            <div class="form-group">
                <label for="email">E-MAIL</label>
                <input type="email" class="form-control" id="email" name="email" />
            </div>
            <div class="form-group">
                <label for="mensagem">MENSAGEM</label>
                <textarea class="form-control" id="mensagem" name="mensagem"></textarea>
            </div>

            <div class="col-xs-7">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="chk" name="enviarCopia" /> Envie com cópia para você
                    </label>
                </div>
            </div>
            <div class="col-xs-5 text-right" style="padding-right: 0;">
                <button type="submit" class="btn btn-default btnSubmit">ENVIAR MENSAGEM</button>
            </div>
        </form>
    </div>
    <div class="col-xs-6">
        <h4 class="headDivider" ><span style="padding-left: 0px; color:#333;">LOCALIZAÇÃO</span></h4>
        <?php echo $localizacao; ?>
        <p>
            <img src="/img/facebook.png">
        </p>
        <h4 class="headDivider" ><span style="padding-left: 0px; color:#333;">RECEBA NOSSAS OFERTAS POR E-MAIL</span></h4>

        <form role="form" action="/contato/newsletter" method="POST">
            <div class="form-group">
                <label for="nome">NOME</label>
                <input type="nome" class="form-control" id="nome" name="nome" />
            </div>
            <div class="form-group">
                <label for="email">E-MAIL</label>
                <input type="email" class="form-control" id="email" name="email" />
            </div>

            <div class="col-xs-12 text-right" style="padding-right: 0;">
                <button type="submit" class="btn btn-default btnSubmit">CADASTRAR</button>
            </div>
        </form>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>


    $(document).ready(function() {

        google.maps.event.addDomListener(window, 'load', function() {

            var mapOptions = {
                zoom: 16,
                center: new google.maps.LatLng(-19.846095, -43.928730)
            };

            var map = new google.maps.Map(document.getElementById('single_map_canvas'),
                    mapOptions);

            new google.maps.Marker({
                position: new google.maps.LatLng(-19.846095, -43.928730),
                map: map,
                title: 'D\'jur Modas'
            });
        });


    });
</script>