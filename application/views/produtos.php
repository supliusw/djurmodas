<link rel="stylesheet" href="/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script src="/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $("a[rel^='prettyPhoto']").prettyPhoto({
            social_tools: ''
        });
    });
</script>

<div class="row" style="margin-top: 40px;">
    <div class="col-xs-12">
        <h2>Produtos</h2>
        <p>
            Fundada em 2001 a D´Jur Modas é fruto de muito trabalho e perseverança do casal Marcos e Judith, que devido a dificuldade de se colocarem no mercado de trabalho na época, resolveram unir seus conhecimentos e aptidões em um projeto familiar. 
        </p>
    </div>
</div>

<div class="row produtos" style="margin-top: 20px;">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="active"><a href="#todos" role="tab" data-toggle="tab">Todos</a></li>
        <li><a href="#cotton" role="tab" data-toggle="tab">Cotton</a></li>
        <li><a href="#liganete" role="tab" data-toggle="tab">Liganete</a></li>
        <li><a href="#camisolas" role="tab" data-toggle="tab">Camisolas e Pijamas</a></li>
        <li><a href="#lingerie" role="tab" data-toggle="tab">Lingerie</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="todos">
            <?php $count = 0; ?>
            <?php foreach ($produtos as $categoria => $arrayProdutos) { ?>
                <?php foreach ($arrayProdutos as $produto) { ?>
                    <?php echo $count == 0 ? '<div class="row">' : ''; ?>
                    <div class="col-xs-3 produto"  style="text-align: center">
                        <a href="<?php echo $produto['path']; ?>" rel="prettyPhoto[todos]">
                            <img style="width: 180px; height: 242px;" src="<?php echo $produto['path']; ?>" />    
                        </a>
                        <p class="playfairDisplay" style="margin-top: 10px;"><?php echo $produto['nome']; ?></p>
                        <a href="/produtos/detalhe/<?php echo $produto['idProdutoFoto']; ?>" class="btnSaibaMais">SAIBA MAIS</a>
                    </div>
                    <?php
                    $count++;
                    if ($count == 4) {
                        echo '</div>';
                        $count = 0;
                    }
                    ?>
                <?php } ?>
            <?php } ?>
            <?php echo $count != 0 ? '</div>' : ''; ?>
        </div>

        <div class="tab-pane" id="cotton">
            <?php $count = 0; ?>
            <?php foreach ($produtos['cotton'] as $produto) { ?>
                <?php echo $count == 0 ? '<div class="row">' : ''; ?>
                <div class="col-xs-3 produto"  style="text-align: center">
                    <a href="<?php echo $produto['path']; ?>" rel="prettyPhoto[cotton]">
                        <img style="width: 180px; height: 242px;" src="<?php echo $produto['path']; ?>" />    
                    </a> 
                    <p class="playfairDisplay" style="margin-top: 10px;"><?php echo $produto['nome']; ?></p>
                    <a href="/produtos/detalhe/<?php echo $produto['id']; ?>" class="btnSaibaMais">SAIBA MAIS</a>    
                </div>
                <?php
                $count++;
                if ($count == 4) {
                    echo '</div>';
                    $count = 0;
                }
                ?>
            <?php } ?>
            <?php echo $count != 0 ? '</div>' : ''; ?>
        </div>

        <div class="tab-pane" id="liganete">
            <?php $count = 0; ?>
            <?php foreach ($produtos['liganete'] as $produto) { ?>
                <?php echo $count == 0 ? '<div class="row">' : ''; ?>
                <div class="col-xs-3 produto"  style="text-align: center">
                    <a href="<?php echo $produto['path']; ?>" rel="prettyPhoto[liganete]">
                        <img style="width: 180px; height: 242px;" src="<?php echo $produto['path']; ?>" />    
                    </a>
                    <p class="playfairDisplay" style="margin-top: 10px;"><?php echo $produto['nome']; ?></p>
                    <a href="/produtos/detalhe/<?php echo $produto['id']; ?>" class="btnSaibaMais">SAIBA MAIS</a>   
                </div>
                <?php
                $count++;
                if ($count == 4) {
                    echo '</div>';
                    $count = 0;
                }
                ?>
            <?php } ?>
            <?php echo $count != 0 ? '</div>' : ''; ?>
        </div>

        <div class="tab-pane" id="camisolas">
            <?php $count = 0; ?>
            <?php foreach ($produtos['pijamas'] as $produto) { ?>
                <?php echo $count == 0 ? '<div class="row">' : ''; ?>
                <div class="col-xs-3 produto"  style="text-align: center">
                    <a href="<?php echo $produto['path']; ?>" rel="prettyPhoto[pijamas]">
                        <img style="width: 180px; height: 242px;" src="<?php echo $produto['path']; ?>" />    
                    </a>   
                    <p class="playfairDisplay" style="margin-top: 10px;"><?php echo $produto['nome']; ?></p>
                    <a href="/produtos/detalhe/<?php echo $produto['id']; ?>" class="btnSaibaMais">SAIBA MAIS</a>       
                </div>         
                <?php
                $count++;
                if ($count == 4) {
                    echo '</div>';
                    $count = 0;
                }
                ?>
            <?php } ?>
            <?php echo $count != 0 ? '</div>' : ''; ?>
        </div>

        <div class="tab-pane" id="lingerie">
            <?php $count = 0; ?>
            <?php foreach ($produtos['lingerie'] as $produto) { ?>
                <?php echo $count == 0 ? '<div class="row">' : ''; ?>
                <div class="col-xs-3 produto"  style="text-align: center">
                    <a href="<?php echo $produto['path']; ?>" rel="prettyPhoto[lingerie]">
                        <img style="width: 180px; height: 242px;" src="<?php echo $produto['path']; ?>" />    
                    </a>   
                    <p class="playfairDisplay" style="margin-top: 10px;"><?php echo $produto['nome']; ?></p>
                    <a href="/produtos/detalhe/<?php echo $produto['id']; ?>" class="btnSaibaMais">SAIBA MAIS</a>  
                </div>            
                <?php
                $count++;
                if ($count == 4) {
                    echo '</div>';
                    $count = 0;
                }
                ?>
            <?php } ?>
            <?php echo $count != 0 ? '</div>' : ''; ?>
        </div>

    </div>



    <script>
        $(function() {
            $('#myTab a').click(function(e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#myTab a[href="' + hash + '"]').tab('show');
        });
    </script>