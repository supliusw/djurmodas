<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Escolher perfil</title>

        <!-- Core CSS - Include with every page -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- SB Admin CSS - Include with every page -->
        <link href="/css/sb-admin.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Escolher perfil:</h3>
                        </div>
                        <div class="panel-body">
                            <?php if (isset($alert)) { ?>
                                <div class="row-fluid">
                                    <div class="alert span11 <?php echo $alert['tipo']; ?>">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert['mensagem']; ?>
                                    </div>
                                </div>
                            <?php } ?>

                            <p>Escolha abaixo com qual perfil deseja acessar o sistema:</p>

                            <form id="login-form">
                                <fieldset>
                                    <div class="form-group">
                                        <select id="selectBox" class="form-control">
                                            <option>Selecione</option>
                                            <?php foreach ($usuarioPerfis as $perfil => $perfis) { ?>
                                                <optgroup label="<?php echo $perfil ?>">
                                                    <?php foreach ($perfis as $key => $row) {  ?>
                                                        <option value="<?php echo $row['id'] ?>"><?php echo $row['fk_pessoa']['detail']['nome'] ?></option>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>               
                                        </select>
                                    </div>
                                    <button type="button" id="entrar-btn" class="btn btn-primary btn-block">Entrar</button>
                                    <a href="/common/login/logout" class="btn btn-danger btn-block">Sair</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Core Scripts - Include with every page -->
        <script src="/js/jquery-1.10.2.js"></script>
        <script src="/js/bootstrap.min.js"></script>

        <script>
            $(function() {
                $("#entrar-btn").click(function() {
                    var perfil = $('#selectBox :selected').parent().attr('label');
                    var id = $('#selectBox').val();
                    if (id > 0)
                        window.location = "/common/login/escolherPerfil/" + perfil + "/" + id;
                    else
                        alert('Selecione um perfil');
                });
            });
        </script>

    </body>

</html>
