<form role="form" method="POST" action="/common/endereco/salvar">
    <input type="hidden" id="idEndereco" name="idEndereco" value="<?php echo $post['id'] ?>">
    <input type="hidden" name="fk_pessoa" value="<?php echo $pessoa['id'] ?>">
    <div class="form-group">
        <div class="controls form-inline">
            <input type="cep" class="form-control cep" id="cep" name="cep" placeholder="CEP" value="<?php echo $post['cep']; ?>">
            <button type="button" class="btn btn-primary" id="btPesquisarCep">Pesquisar</button>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <input type="text" id="logradouro" name="logradouro" class="form-control" placeholder="Logradouro" value="<?php echo $post['logradouro']; ?>">
            </div>
            <div class="col-xs-3">
                <input type="text" id="numero" name="numero" class="form-control" placeholder="Número" value="<?php echo $post['numero']; ?>">
            </div>
            <div class="col-xs-3">
                <input type="text" id="complemento" name="complemento" class="form-control" placeholder="Complemento" value="<?php echo $post['complemento']; ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <input type="text" name="bairro" id="bairro" name="bairro" class="form-control" placeholder="Bairro" value="<?php echo $post['bairro']; ?>" /> 
            </div>
            <div class="col-xs-2">
                <select name="uf" id="uf" class="form-control">
                    <option value="">UF</option>
                </select>
            </div>
            <div class="col-xs-4">
                <select name="fk_cidade" id="fk_cidade" class="form-control" value="<?php echo $post['fk_cidade']; ?>">
                    <option value="">Cidade</option>
                </select>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Salvar</button>
</form>

<br />

<table class="table table-condensed">
    <thead>
        <tr>
            <th>Principal</th>
            <th>Endereço</th>
            <th colspan="2">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($enderecos) > 0) {
            foreach ($enderecos as $endereco) {
                ?>
                <tr>
                    <td>
                        <input itemid="<?php echo $endereco['id'] ?>" type="radio" data-size="mini" name="enderecoPrincipal" <?php echo ($endereco['id'] == $pessoa['fk_endereco']['id']) ? 'checked' : '' ?> class="toggle enderecoPrincipal" data-on-color="success" data-off-color="default" data-on-text="Sim" data-off-text="Não">
                    </td>
                    <td><?php echo $endereco['logradouro'] . ' ' . $endereco['numero'] . ' ' . $endereco['complemento'] . ' - ' . $endereco['bairro'] . ' - CEP: ' . format_mask($endereco['cep'], MASK_CEP) . ' - ' . $endereco['fk_cidade']['detail']['nome'] . '/' . $endereco['fk_cidade']['detail']['uf'] ?></td>
                    <td align="center" width="20">
                        <button type="button" class="btn editEndereco btn-xs btn-primary" itemref='<?php echo json_encode($endereco) ?>' ><span class="fa fa-pencil"></span></a>
                    </td>
                    <td align="center" width="20">
                        <a href="/common/endereco/excluir/<?php echo $endereco['id'] ?>" class="btn btn-xs btn-danger"><span class="fa fa-trash-o"></span></a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="4">Nenhum endereço cadastrado</td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script>
    $(function() {
        loadUF($("#uf"), $("#fk_cidade"));

        $(".enderecoPrincipal").on('switchChange', function(e, data) {
            $.post('/common/pessoa/setParamPessoa/<?php echo $pessoa['id'] ?>', {fk_endereco: data.value ? $(this).attr('itemid') : 'null'});
        });

        $("#btPesquisarCep").click(function() {
            var t = $(this);
            t.html('Conectando site dos Correios ..');

            var cep = $("input[name=cep]");

            var logradouro = $("input[name=logradouro]");
            logradouro.val('');

            var bairro = $("input[name=bairro]");
            bairro.val('');

            var cidade = $("select[name=fk_cidade]");
            cidade.attr('itemname', '');

            $.post('/common/util/consultaCep', {cep: cep.val()}, function(json) {
                t.html('Pesquisar');

                if (json.logradouro.length > 0) {
                    logradouro.val(json.logradouro);
                    bairro.val(json.bairro);
                    cidade.attr('itemid', '');
                    cidade.attr('itemname', json.cidade.toUpperCase());
                    $("#uf option").each(function() {
                        if ($(this).val() === json.uf) {
                            $(this).attr("selected", "true");
                            $(this).trigger("change");
                        }
                    });
                } else
                    cepErroLabel.css('display', '');
                gifLoading.css('display', 'none');
            }, 'json');
        });

        $(".editEndereco").click(function() {
            var json = jQuery.parseJSON($(this).attr('itemref'));

            $("#idEndereco").val(json.id);
            $("#logradouro").val(json.logradouro);
            $("#numero").val(json.numero);
            $("#complemento").val(json.complemento);
            $("#bairro").val(json.bairro);
            $("#tipo").val(json.tipo);
            $("#cep").val(json.cep);
            $("#cep").mask('99.999-999');
            $("#fk_cidade").attr('itemid', json.fk_cidade.detail.id);
            $("#uf").val(json.fk_cidade.detail.uf);
            $("#uf").trigger('change');
        });

    });
</script>