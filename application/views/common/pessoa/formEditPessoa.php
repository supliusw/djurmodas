<div class="form-group">
    <label>Tipo:</label>    
    <select class="form-control" id="tipo_pessoa">
        <option value="PF" <?php echo strlen($pessoa['documento']) == 11 ? 'selected' : '' ?>>Pessoa física</option>
        <option value="PJ" <?php echo strlen($pessoa['documento']) == 14 ? 'selected' : '' ?>>Pessoa jurídica</option>
    </select>
</div>

<form role="form" class="formEdit" itemref="common/pessoa" itemid="<?php echo $pessoa['id'] ?>">
    <div class="form-group">
        <label>Nome:</label>
        <input class="form-control" name="nome" value="<?php echo $pessoa['nome'] ?>">
    </div>
    <div class="form-group">
        <label id="labelRazaoApelido"><?php echo strlen($pessoa['documento']) == 14 ? 'Razão Social:' : 'Apelido:' ?></label>
        <input class="form-control" name="razao_apelido" value="<?php echo $pessoa['razao_apelido'] ?>">
    </div>
    <div class="form-group">
        <label>Documento:</label>
        <input class="form-control <?php echo strlen($pessoa['documento']) == 14 ? 'cnpj' : 'cpf' ?>" id="documento_pessoa" name="documento" value="<?php echo $pessoa['documento'] ?>">
    </div>                    
</form>

<script>
    $(function() {
        $("#tipo_pessoa").change(function() {
            var txtDocumento = $('#documento_pessoa');
            txtDocumento.removeClass('cpf');
            txtDocumento.removeClass('cnpj');

            if ($(this).val() === 'PF') {
                $("#labelRazaoApelido").html('Apelido:');
                txtDocumento.addClass('cpf');
                $('.cpf').mask('000.000.000-00', {reverse: true, onChange: changeFields});
            }

            if ($(this).val() === 'PJ') {
                $("#labelRazaoApelido").html('Razão social:');
                txtDocumento.addClass('cnpj');
                $('.cnpj').mask('00.000.000/0000-00', {reverse: true, onChange: changeFields});
            }
        });
    });
</script>