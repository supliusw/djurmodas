<br />

<ol class="breadcrumb">
    <li><a href="#">Cadastros</a></li>
    <li><a href="#"><?php echo $tipo ?></a></li>
    <li class="active">Criar</li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Cadastrar <?php echo $tipo ?>:
            </div>
            <div class="panel-body">

                <div class="form-group">
                    <label>Tipo:</label>
                    <select class="form-control" id="tipo_pessoa">
                        <option value="PF" <?php echo count($post['documento']) == 11 ? 'selected' : '' ?>>Pessoa física</option>
                        <option value="PJ" <?php echo count($post['documento']) == 14 ? 'selected' : '' ?>>Pessoa jurídica</option>
                    </select>
                </div>

                <form role="form" action="/common/pessoa/cadastrar" method="POST">
                    <input type="hidden" value="<?php echo $tipo ?>" id="tipo" name="tipo" />

                    <div class="form-group">
                        <label>Documento:</label>
                        <input class="form-control <?php echo strlen($post['documento']) == 14 ? 'cnpj' : 'cpf' ?>" id="documento_pessoa" name="documento" value="<?php echo $post['documento'] ?>">
                    </div>    

                    <div class="form-group">
                        <label>Nome:</label>
                        <input class="form-control" name="nome" value="<?php echo $post['nome'] ?>">
                    </div>
                    <div class="form-group">
                        <label id="labelRazaoApelido"><?php echo strlen($post['documento']) == 14 ? 'Razão Social:' : 'Apelido:' ?></label>
                        <input class="form-control" name="razao_apelido" value="<?php echo $post['razao_apelido'] ?>">
                    </div>  

                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                    <button type="reset" class="btn btn-danger">Limpar</button>
                </form>

                <div class="row" style="padding-left: 15px; padding-right: 15px; margin-top: 20px;">
                    <div class="alert col-lg-12 alert-warning">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        Atenção: Informações adicionais serão cadastradas na próxima tela
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="pessoaExisteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">O documento informado já existe!</h4>
            </div>
            <form role="form" method="POST" action="/common/pessoa/vincular">
                <input type="hidden" id="pessoaExisteModal_fkPessoa" name="fk_pessoa" />
                <input type="hidden" value="<?php echo $tipo ?>" id="tipo" name="tipo" />

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning">
                                Encontramos um cadastro com o documento <span id="pessoaExisteModal_documento_span"></span>.<br />
                                <span style="font-size: 11px;">(OBS: O sistema não permite cadastros com documentos repetidos)</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nome:</label>
                        <div id="pessoaExisteModal_nome_div"></div>
                    </div>

                    <div class="form-group">
                        <label>Razão Social/Apelido:</label>
                        <div id="pessoaExisteModal_razao_div"></div>
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="submit" value="Utilizar" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#tipo_pessoa").change(function() {
            var txtDocumento = $('#documento_pessoa');
            txtDocumento.removeClass('cpf');
            txtDocumento.removeClass('cnpj');

            if ($(this).val() === 'PF') {
                $("#labelRazaoApelido").html('Apelido:');
                txtDocumento.addClass('cpf');
                $('.cpf').mask('000.000.000-00', {reverse: true, onChange: changeFields});
            }

            if ($(this).val() === 'PJ') {
                $("#labelRazaoApelido").html('Razão social:');
                txtDocumento.addClass('cnpj');
                $('.cnpj').mask('00.000.000/0000-00', {reverse: true, onChange: changeFields});
            }
        });

        $("#documento_pessoa").change(function() {
            var documento = $(this).val();
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: '/common/pessoa/checkDocumento',
                data: {documento: documento},
                success: function(json) {
                    if (json.code == 0) {
                        $("#pessoaExisteModal_documento_span").html(documento);
                        $("#pessoaExisteModal_nome_div").html(json.pessoa.nome);
                        $("#pessoaExisteModal_razao_div").html(json.pessoa.razao_apelido);
                        $("#pessoaExisteModal_fkPessoa").val(json.pessoa.id);
                        

                        $('#pessoaExisteModal').modal('show');
                    }
                }
            });
        });
    });
</script>