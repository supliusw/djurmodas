
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Djur Admin</title>

        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/style.css">

    </head>

    <body>
        <br />
        
        <div class="container">
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-4">
                        &nbsp;
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-user"></i> Login</h3>
                            </div>
                            <div class="panel-body">
                                <?php if (isset($alert)) { ?>
                                    <div class="row-fluid">
                                        <div class="alert span11 <?php echo $alert['tipo']; ?>">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <?php echo $alert['mensagem']; ?>
                                        </div>
                                    </div>
                                <?php } ?>

                                <form id="login-form" action="/common/login/acessar" class="form" method="POST">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Usuário" name="usuario" value="" autofocus />
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Senha" name="senha" type="password" value="">
                                        </div>
                                        <button type="submit" id="login-btn" class="btn btn-primary btn-block">Entrar</button>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        &nbsp;
                    </div>
                </div><!-- /.row -->
            </div>

        </div><!-- /.container -->


        <!-- JavaScript -->
        <script src="/js/jquery-1.10.2.js"></script>
        <script src="/js/bootstrap.js"></script>
    </body>
</html>