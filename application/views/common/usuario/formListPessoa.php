<form role="form" method="POST" action="/common/usuario/salvar">
    <input type="hidden" id="idUsuario" name="idUsuario" value="<?php echo $post['idUsuario'] ?>">
    <input type="hidden" name="<?php echo $this->router->class == 'meusDados' ? 'fk_' . strtolower($this->usuarioPerfil) : 'fk_' . $this->router->class ?>" value="<?php echo $idRef ?>">
    <div class="form-group">
        <div class="row">
            <?php if ($this->router->class == 'meusDados') { ?>
                <div class="col-xs-4">
                    <input type="text" id="u_usuario" name="usuario" class="form-control" placeholder="Usuário" value="<?php echo $post['usuario']; ?>">
                </div>
                <div class="col-xs-4">
                    <input type="password" id="u_senha" name="senha" class="form-control" placeholder="Senha" value="">
                </div>
                <div class="col-xs-4">
                    <select class="form-control" id="u_fk_grupo" name="fk_grupo">
                        <option value="null">Grupo de permissão</option>
                        <?php if (isset($gruposPermissao) && count($gruposPermissao) > 0) { ?>
                            <?php foreach ($gruposPermissao as $row) { ?>
                                <option value="<?php echo $row['id'] ?>" <?php echo $row['id'] == $dados['fk_grupo']['id'] ? 'selected' : '' ?>><?php echo $row['nome'] ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            <?php } else { ?>
                <div class="col-xs-6">
                    <input type="text" id="u_usuario" name="usuario" class="form-control" placeholder="Usuário" value="<?php echo $post['usuario']; ?>">
                </div>
                <div class="col-xs-6">
                    <input type="password" id="u_senha" name="senha" class="form-control" placeholder="Senha" value="">
                </div>
            <?php } ?>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Salvar</button>
</form>
<hr />

<table class="table table-condensed">
    <thead>
        <tr>
            <th>Usuário</th>
            <?php if ($this->router->class == 'meusDados') { ?>
                <th>Grupo de permissão</th>
            <?php } ?>
            <th colspan="3">Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($usuarios) > 0) {
            foreach ($usuarios as $user) {
                ?>
                <tr>
                    <td><?php echo $user['usuario']; ?></td> 

                    <?php if ($this->router->class == 'meusDados') { ?>
                        <?php if (is_null($user['grupo'])) { ?>
                            <td><span class="label label-default">Acesso total</span></td> 
                        <?php } else { ?>
                            <td><?php echo $user['grupo']; ?></td> 
                        <?php } ?>
                    <?php } ?>

                    <td width="50">
                        <?php if ($user['fk_usuario'] != $this->usuario['id']) { ?>
                            <input itemid="<?php echo $user['fk_usuario_perfil'] ?>" type="checkbox" class="toggle statusUsuario" <?php echo ($user['ativo_usuario_perfil'] == 't') ? 'checked' : '' ?> data-on-color="success" data-size="mini" data-off-color="warning" data-on-text="Ativo" data-off-text="Inativo" />
                        <?php } else { ?>
                            <i class="fa fa-lock"></i>
                        <?php } ?>
                    </td> 

                    <?php if ($user['fk_usuario'] != $this->usuario['id']) { ?>
                        <td align="center" width="20">

                            <a type="button" class="btn editUsuario btn-xs btn-primary" itemref='<?php echo json_encode($user) ?>' ><span class="fa fa-pencil"></span></a>
                        </td>
                        <td align="center" width="20">
                            <a href="/common/usuario/excluir/<?php echo $user['fk_usuario'] ?>" class="btn btn-xs btn-danger"><span class="fa fa-trash-o"></span></a>

                        </td>
                    <?php } else { ?>
                        <td align="center" colspan="2" width="20">
                            <i class="fa fa-lock"></i>
                        </td>
                    <?php } ?>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="3">Nenhum usuário cadastrado</td>
            </tr>
        <?php } ?>

    </tbody>
</table>

<script>
    $(function() {

        $(".statusUsuario").on('switchChange', function(e, data) {
            $.post('/usuario/statusUsuarioPerfil/' + $(this).attr('itemid'), {ativo: data.value});
        });

        $(".editUsuario").click(function() {
          
            var json = jQuery.parseJSON($(this).attr('itemref'));

            $("#idUsuario").val(json.fk_usuario);
            $("#u_usuario").val(json.usuario);
            
            <?php if ($this->router->class == 'meusDados') { ?>
                $("#u_fk_grupo").val(json.fk_grupo);
            <?php } ?>
        });

    });
</script>