<br />

<ol class="breadcrumb">
    <li><a href="#">Configurações</a></li>
    <li><a href="#">Usuário</a></li>
</ol>

<?php if (isset($alert)) { ?>
    <div class="row" style="padding-left: 15px; padding-right: 15px;">
        <div class="alert col-lg-12 <?php echo $alert['tipo']; ?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert['mensagem']; ?>
        </div>
    </div>
<?php } ?>


<div class="row">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dados do usuário:
            </div>
            <div class="panel-body">
                <form role="form" class="formEdit" itemref="common/meusDados" itemid="<?php echo $user['id'] ?>">
                    <div class="form-group">
                        <label>Usuário:</label>
                        <input class="form-control" name="usuario" value="<?php echo $user['usuario'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Nova Senha:</label>
                        <input class="form-control" name="senha" />
                    </div>                   
                </form>
            </div>
        </div>

    </div>


</div>


<script>
    $(function() {

    });
</script>