<form role="form" method="POST" action="/common/contato/salvar">
    <input type="hidden" id="idContato" name="idContato" value="<?php echo $post['idContato'] ?>">
    <input type="hidden" name="fk_pessoa" value="<?php echo $pessoa['id'] ?>">
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <input type="text" id="responsavel" name="responsavel" class="form-control" placeholder="Nome do responsável" value="<?php echo $post['responsavel']; ?>">
            </div>
            <div class="col-xs-6">
                <input type="text" id="email" name="email" class="form-control" placeholder="Email" value="<?php echo $post['email']; ?>">
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <input type="text" id="telefone" name="telefone" class="form-control telefone" placeholder="Telefone" value="<?php echo $post['telefone']; ?>">
            </div>
            <div class="col-xs-6">
                <input type="text" id="celular" name="celular" class="form-control telefone" placeholder="Celular" value="<?php echo $post['celular']; ?>">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Salvar</button>
</form>

<br />

<table class="table table-condensed">
    <thead>
        <tr>
            <th>Principal</th>
            <th>Responsável</th>
            <th>Email</th>
            <th>Telefone</th>
            <th>Celular</th>
            <th colspan="2">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($contatos) > 0) {
            foreach ($contatos as $contato) {
                ?>
                <tr>
                    <td><input itemid="<?php echo $contato['id'] ?>" type="radio" data-size="mini" name="contatoPrincipal" <?php echo ($contato['id'] == $pessoa['fk_contato']['id']) ? 'checked' : '' ?> class="toggle contatoPrincipal" data-on-color="success" data-off-color="default" data-on-text="Sim" data-off-text="Não"></td> 
                    <td><?php echo $contato['responsavel']; ?></td> 
                    <td><?php echo $contato['email']; ?></td>
                    <td><?php echo format_mask($contato['telefone'], MASK_TELEFONE); ?></td>
                    <td><?php echo format_mask($contato['celular'], MASK_TELEFONE); ?></td>

                    <td align="center" width="20">
                        <button type="button" class="btn editContato btn-xs btn-primary" itemref='<?php echo json_encode($contato) ?>' ><span class="fa fa-pencil"></span></a>
                    </td>
                    <td align="center" width="20">
                        <a href="/common/contato/excluir/<?php echo $contato['id'] ?>" class="btn btn-xs btn-danger"><span class="fa fa-trash-o"></span></a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="7">Nenhum contato cadastrado</td>
            </tr>
        <?php } ?>

    </tbody>
</table>


<script>
    $(function() {
        $(".contatoPrincipal").on('switchChange', function(e, data) {
            $.post('/common/pessoa/setParamPessoa/<?php echo $pessoa['id'] ?>', {fk_contato: data.value ? $(this).attr('itemid') : 'null'});
        });


        $(".editContato").click(function() {
            var json = jQuery.parseJSON($(this).attr('itemref'));

            $("#idContato").val(json.id);
            $("#responsavel").val(json.responsavel);
            $("#email").val(json.email);
            $("#telefone").val(json.telefone);
            $("#celular").val(json.celular);

            var masks = ['(00) 00000-0000', '(00) 0000-00009'],
                    maskBehavior = function(val, e, field, options) {
                        return val.length > 14 ? masks[0] : masks[1];
                    };

            $('.telefone').mask(maskBehavior, {onKeyPress:
                        function(val, e, field, options) {
                            field.mask(maskBehavior(val, e, field, options), options);
                        }
            });
        });

    });
</script>