<?php

if (isset($argv[1]))
    $tabela_especifica = $argv[1];

define('BASEPATH', '');

define('PATHMODELS', dirname(__FILE__) . '/application/models');

define('ENVIRONMENT', 'development');

include('application/config/' . ENVIRONMENT . '/database.php');

println("Reverse Engineer..");

$str = getDriver($db['default']['dbdriver']) . ':host=' . $db['default']['hostname'] . ';' . (isset($db['default']['port']) ? 'port=' . $db['default']['port'] . ';' : '') . 'dbname=' . $db['default']['database'];

println("Conectando a base de dados ($str) ");
$conn = new PDO($str, $db['default']['username'], $db['default']['password']);

$controllersFiles = array_diff(scandir('application/controllers/admin'), array('..', '.'));
foreach ($controllersFiles as $row) {
    $f = str_replace('.php', '', $row);

    $query = $conn->query("SELECT id FROM tb_controller WHERE controller = '$f'");
    $controllers = $query->fetchAll();

    if (count($controllers) == 0) {
        $conn->query("INSERT INTO tb_controller (controller, nome) VALUES ('$f', '$f')");
        println("Registro '$f' criado em tb_controller");
    } else {
        println("Registro '$f' ja existe em tb_controller");
    }
}

$tables = descTables($db['default']['dbdriver']);

foreach ($tables as $table => $columns) {

    $bufferFields = "";
    foreach ($columns as $column => $data) {
        $bufferFields .= '                "' . $column . '" => array(' . PHP_EOL;
        $bufferFields .= '                    "label" => "' . $column . '",' . PHP_EOL;
        $bufferFields .= '                    "rules" => "' . $data['rules'] . '",' . PHP_EOL;
        $bufferFields .= isset($data['fk_table_model']) ? ('                    "fk_table_model" => "' . $data['fk_table_model'] . '",' . PHP_EOL) : '';
        $bufferFields .= '                    "after_send" => ""' . PHP_EOL;
        $bufferFields .= '                ),' . PHP_EOL;
    }

    //Definindo nome do arquivo..
    $file = PATHMODELS . '/' . strtolower($table) . '_model.php';

    if (!file_exists($file)) {

        $buffer = '<?php' . PHP_EOL;
        $buffer .= '    if (!defined(\'BASEPATH\'))' . PHP_EOL;
        $buffer .= '        exit(\'No direct script access allowed\');' . PHP_EOL . PHP_EOL;

        $buffer .= '    class ' . ucfirst($table) . '_model extends SModel {' . PHP_EOL . PHP_EOL;

        $buffer .= '        function __construct() {' . PHP_EOL;
        $buffer .= '            parent::__construct();' . PHP_EOL;

        $buffer .= '            $this->table = "' . $table . '";' . PHP_EOL;
        $buffer .= '            $this->table_label = "' . camelize($table) . '";' . PHP_EOL . PHP_EOL;

        $buffer .= '            $this->fields = array(' . PHP_EOL . $bufferFields . '            );' . PHP_EOL;

        $buffer .= '        }' . PHP_EOL;
        $buffer .= '    }' . PHP_EOL;

        println("Criando arquivo: $file");
        file_put_contents($file, $buffer);
    } else {

        $read = file($file); // and this one

        $posThisFields = null;

        $unset = 0;
        foreach ($read as $lineNum => $row) {
            if (strstr($row, '$this->fields') && $unset != 2) {
                $posThisFields = $lineNum;
                $unset = 1;
            }

            if ($unset == 1 && $posThisFields != $lineNum) {
                unset($read[$lineNum]);
                if (strstr($row, ');'))
                    $unset = 2;
            }
        }

        $read[$posThisFields] = '            $this->fields = array(' . PHP_EOL . $bufferFields . '            );' . PHP_EOL;

        file_put_contents($file, $read); // overwrite the file

        println("Atualizando arquivo: $file");
    }
}



/*
 * Functions..
 */

function println($string) {
    echo $string . PHP_EOL;
}

function getDriver($str) {
    switch ($str) {
        case 'postgre':
            return 'pgsql';
        default:
            break;
    }
    return $str;
}

function descTables($driver) {
    global $conn, $tabela_especifica;

    $tables = array();

    switch ($driver) {
        case 'mysql':
            $query = $conn->query('SHOW TABLES');

            while (( $table = $query->fetchColumn(0) ) !== false) {

                if (isset($tabela_especifica) && $tabela_especifica != $table)
                    continue;

                println('Verificando tabela ' . $table);

                $qry = $conn->query('SHOW COLUMNS FROM ' . $table);

                $qry2 = $conn->query("SELECT `column_name`, 
                                             `referenced_table_schema` AS foreign_db, 
                                             `referenced_table_name` AS foreign_table, 
                                             `referenced_column_name`  AS foreign_column 
                                      FROM `information_schema`.`KEY_COLUMN_USAGE`
                                      WHERE `table_name` = '$table'
                                      AND `referenced_column_name` IS NOT NULL
                                      ORDER BY `column_name`");

                if ($qry !== false && $qry2 !== false) {
                    $columns = $qry->fetchAll();
                    $fks_uniques = $qry2->fetchAll();

                    foreach ($columns as $column) {
                        $tables[$table][$column["Field"]]['rules'] = convertRules($column);

                        foreach ($fks_uniques as $row) {
                            if ($column["Field"] == $row['column_name']) {
                                $tables[$table][$column["Field"]]['fk_table_model'] = ucfirst($row['foreign_table']) . '_model';
                                break;
                            }
                        }
                    }
                }
            }
            break;

        case 'postgre':
            $query = $conn->query("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'");

            while (( $table = $query->fetchColumn(0) ) !== false) {

                if (isset($tabela_especifica) && $tabela_especifica != $table)
                    continue;

                println('Verificando tabela ' . $table);

                $qry = $conn->query("SELECT * FROM information_schema.columns WHERE table_name ='$table' ORDER BY ordinal_position");

                $qry2 = $conn->query("SELECT constraint_type, kcu.column_name, ccu.table_name AS foreign_table_name
                                      FROM information_schema.table_constraints AS tc 
                                      JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name
                                      JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name
                                      WHERE constraint_type = 'FOREIGN KEY'  AND tc.table_name='$table'");

                if ($qry !== false && $qry2 !== false) {
                    $columns = $qry->fetchAll();
                    $fks_uniques = $qry2->fetchAll();


                    foreach ($columns as $column) {
                        $tables[$table][$column["column_name"]]['rules'] = convertRules($column);

                        foreach ($fks_uniques as $row) {
                            if ($column["column_name"] == $row['column_name']) {
                                $tables[$table][$column["column_name"]]['fk_table_model'] = ucfirst($row['foreign_table_name']) . '_model';
                                break;
                            }
                        }
                    }
                }
            }
            break;


        default:
            break;
    }

    return $tables;
}

function convertRules($column) {
    $rule = "";

    if ((isset($column['Key']) && $column['Key'] == 'PRI') || (isset($column['column_default']) && strpos($column['column_default'], 'nextval') !== false))
        $rule .= "primary_key|";
    else if ((isset($column['Null']) && $column['Null'] == 'NO') || (isset($column['is_nullable']) && $column['is_nullable'] == 'NO'))
        $rule .= "required|";

    if (isset($column['Type'])) {
        $strpos = strpos($column['Type'], 'varchar(');
        if (!($strpos === false)) {
            $l = str_replace('varchar(', '', $column['Type']);
            $rule .= "max_length[" . (rtrim($l, ")")) . "]|";
        }
    }


    if ((isset($column['character_maximum_length']) && $column['character_maximum_length'] > 0))
        $rule .= "max_length[" . $column['character_maximum_length'] . "]|";

    if (isset($column['Type']) || isset($column['data_type'])) {

        $type = isset($column['Type']) ? $column['Type'] : $column['data_type'];

        if (strpos($type, 'tinyint') !== false)
            $rule .= "boolean|";
        elseif (strpos($type, 'int') !== false)
            $rule .= "integer|";

        if (strpos($type, 'decimal') !== false || strpos($type, 'numeric') !== false) {
                $rule .= "decimal|";

            if (strpos($type, 'decimal') !== false) {
                $precision = str_replace('(', '[', $type);
                $precision = str_replace(')', ']', $precision);
                $rule .=  str_replace('decimal', 'precision', $precision).'|';
            } 
        }

        if ($type == 'datetime' || strpos($type, 'timestamp') !== false)
            $rule .= "date_time|";

        if ($type == 'boolean')
            $rule .= "boolean|";

        if ($type == 'date')
            $rule .= "date|";
    }

    return rtrim($rule, "|");
}

function camelize($value) {
    $result = str_replace(
            ' ', '', ucwords(
                    preg_replace('/[^a-z0-9]+/i', ' ', $value)
            )
    );

    return $result;
}

?>
